import React, { useState } from 'react';
import Aside from './Aside';
import Main from './Main';
import TestComponent from "./components/TestComponent";

function Layout() {
  const [rtl, setRtl] = useState(false);
  const [collapsed, setCollapsed] = useState(false);
  const [image, setImage] = useState(true);
  const [toggled, setToggled] = useState(false);

  const handleCollapsedChange = () => {
    setCollapsed(!collapsed);
  };

  const handleRtlChange = (checked) => {
    setRtl(checked);
  };
  const handleImageChange = (checked) => {
    setImage(checked);
  };

  const handleToggleSidebar = (value) => {
    setToggled(!value);
  };

  return (
      <div className={`app ${rtl ? 'rtl' : ''} ${toggled ? 'toggled' : ''}`}>
        {/*<Header*/}
        {/*    toggled={toggled}*/}
        {/*    collapsed={collapsed}*/}
        {/*    handleToggleSidebar={handleToggleSidebar}*/}
        {/*    handleCollapsedChange={handleCollapsedChange}*/}
        {/*/>*/}
        <Aside
            image={image}
            collapsed={collapsed}
            toggled={toggled}
            handleToggleSidebar={handleToggleSidebar}
        />
        <TestComponent
            handleCollapsedChange={handleCollapsedChange}
        />
        <Main
            image={image}
            toggled={toggled}
            collapsed={collapsed}
            rtl={rtl}
            handleToggleSidebar={handleToggleSidebar}
            handleCollapsedChange={handleCollapsedChange}
            handleRtlChange={handleRtlChange}
            handleImageChange={handleImageChange}
        />
      </div>
  );
}

export default Layout;