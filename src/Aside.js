import React from 'react';
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';
import { FaTachometerAlt, FaGem, FaList, FaGithub, FaRegLaughWink, FaHeart } from 'react-icons/fa';

const Aside = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  return (
      <ProSidebar
          rtl={rtl}
          collapsed={collapsed}
          toggled={toggled}
          breakPoint="md"
          onToggle={handleToggleSidebar}>
        <SidebarHeader>
          <div
              style={{
                padding: '24px',
                textTransform: 'uppercase',
                fontWeight: 'bold',
                fontSize: 14,
                letterSpacing: '1px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}>
            SidebarTitle
          </div>
        </SidebarHeader>

        <SidebarContent>
          <Menu iconShape="circle">
            <MenuItem
                icon={<FaTachometerAlt />}
                suffix={<span className="badge red">New</span>}>
              Dashboard
            </MenuItem>
            <MenuItem icon={<FaGem />}> Components</MenuItem>
          </Menu>
          <Menu iconShape="circle">
            <SubMenu
                suffix={<span className="badge yellow">3</span>}
                title={"withSuffix"}
                icon={<FaRegLaughWink />}>
              <MenuItem>Submenu 1</MenuItem>
              <MenuItem>Submenu 2</MenuItem>
              <MenuItem>Submenu 3</MenuItem>
            </SubMenu>
            <SubMenu
                prefix={<span className="badge gray">3</span>}
                title={"Testing123"}
                icon={<FaHeart />}>
              <MenuItem>Submenu 1</MenuItem>
              <MenuItem>Submenu 2</MenuItem>
              <MenuItem>Submenu 3</MenuItem>
            </SubMenu>
            <SubMenu title={"Multi-levl"} icon={<FaList />}>
              <MenuItem>Submenu 1</MenuItem>
              <MenuItem>Submenu 2</MenuItem>
              <SubMenu title={`Submenu 3`}>
                <MenuItem>Submenu 3.1</MenuItem>
                <MenuItem>Submenu 3.2</MenuItem>
                <SubMenu title={`Submenu 3.3`}>
                  <MenuItem>Submenu 3.3.1 </MenuItem>
                  <MenuItem>Submenu 3.3.2 </MenuItem>
                  <MenuItem>Submenu 3.3.3 </MenuItem>
                </SubMenu>
              </SubMenu>
            </SubMenu>
          </Menu>
        </SidebarContent>

        <SidebarFooter style={{ textAlign: 'center' }}>
          <div
              className="sidebar-btn-wrapper"
              style={{
                padding: '20px 24px',
              }}>
            <a
                href="https://github.com/azouaoui-med/react-pro-sidebar"
                target="_blank"
                className="sidebar-btn"
                rel="noopener noreferrer">
              <FaGithub />
              <span>View Source</span>
            </a>
          </div>
        </SidebarFooter>
      </ProSidebar>
  );
};

export default Aside;