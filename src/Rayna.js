//import './RaynaAlt.scss';
import './Rayna.scss';
import React, {useState} from 'react'
import Sidebar from "./components/sidebar/Sidebar";
import Content from "./components/content/Content";
import Navbar from "./components/navbar/Navbar";

// import {ReactComponent as PremiumIcon} from "./svgs/premiumBadge.svg";
// import {ReactComponent as CollapseIcon} from './svgs/collapse.svg';
import MusicControls from "./components/MusicControls";

function Rayna() {
  const [collapsed, setCollapsed] = useState(false);
  // const [image, setImage] = useState(true);
  const [toggled, setToggled] = useState(false);

  const handleCollapsedChange = () => {
    setCollapsed(!collapsed);
  };

  const handleToggleSidebar = (value) => {
    setToggled(!value);
  };

  return (
      <div>
        <Navbar
            handleCollapsedChange={handleCollapsedChange}
            collapsed={collapsed}
        />
        <div className="App">
          {/*<div className={"sidebar"}>*/}
          {/*  <header>Current Server</header>*/}
          {/*  <ul>*/}
          {/*    <li>*/}
          {/*      <a href="#">*/}
          {/*        <div className={"divIcon"}>*/}
          {/*          <PremiumIcon/>*/}
          {/*        </div>*/}
          {/*        <span>Home</span>*/}
          {/*      </a>*/}
          {/*    </li>*/}
          {/*    <li><a href="#">Dashboard</a></li>*/}
          {/*    <li><a href="#">Settings</a></li>*/}
          {/*    <li><a href="#">Support</a></li>*/}
          {/*    <li><a href="#">Feedback</a></li>*/}
          {/*  </ul>*/}
          {/*  <button>*/}
          {/*    <CollapseIcon/>*/}
          {/*    <span>Collapse Sidebar</span>*/}
          {/*  </button>*/}
          {/*</div>*/}
          <Sidebar
            collapsed={collapsed}
            toggled={toggled}
            breakPoint={"md"}
            onToggle={handleToggleSidebar}
            handleCollapsedChange={handleCollapsedChange}
          />
          <Content/>
        </div>
        {/*<Content/>*/}
        <MusicControls/>
      </div>
  );
}

export default Rayna;