import React from 'react';
import {ReactComponent as NotificationsImage} from "../svgs/youtube/notificationSettingsImage.svg";

const NotificationSettingsPage = () => {

  return (
      <div className={"notificationSettingsPage"}>

        <div className={"notificationSettingsHeader"}>
          <div className={"notificationsHeaderContentWrapper"}>
            <div className={"notificationsHeaderContentLeft"}>
              <div>Notification Settings</div>
              <h3>Choose when and how to be notified</h3>
              <span>Select push and email notifications you'd like to receive</span>
            </div>
            <div className={"notificationsHeaderContentRight"}>
              <NotificationsImage/>
            </div>
          </div>
        </div>
        <div className={"notificationSettingsContent"}>
          <div className={"notificationSettingsWrapper"}>
            <div className={"notificationsContentTitle"}>Your preferences</div>
            <div className={"notificationOptionsWrapper"}>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
              <div className={"notificationOptions"}>
                <input type={"checkbox"}/>
                <div>
                  <p>Setting</p>
                  <span>Description</span>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className={"notificationsContentTitle"}>Title</div>
          </div>
        </div>
        <div className={"notificationSettingsFooter"}></div>

      </div>
  );
};

export default NotificationSettingsPage;