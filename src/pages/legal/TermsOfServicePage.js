import React from 'react';

const TermsOfServicePage = () => {
  return (
      <div className="tosPage">
        <div className="tosHeader">
          <h1>Terms of Use</h1>
          <span>Last Updated: Apr 27, 2020</span>
        </div>
        <p>
          Rayna LLC ("us" or "we" or "our") owns and operates the Rayna.io site ("Site") and the Rayna Discord
          Bot ("Bot"). The following Terms of Use (“TOU”) govern your use of the Site and the Bot. Other sites or
          content owned or controlled by Rayna may have their own terms of use and should be reviewed.
        </p>
        <p>
          Rayna may also offer promotions, sweepstakes, contests, services, or features that have their own terms of
          use or rules, and to the extent any portion of those special terms conflict with these TOU, the special terms
          will govern for that specific portion.
        </p>
        <p>
          Using Rayna is at-will and can be terminated at any time by either us or you for any reason. By using
          Rayna, you agree to be bound by these TOU. If you do not agree to be so bound, you are not authorized to
          use the Site or the Bot. These TOU are a legal contract between you and Rayna and govern your access to and
          use of the Site together with any services offered through the Site.
        </p>
        <p>
          Rayna is not affiliated with <a href="https://discordapp.com">Discord</a>, Blizzard, Ubisoft, Steam or any
          game developers of the games we support.
        </p>
        <h2>Rayna</h2>
        <p>The purpose of Rayna is to provide Discord channels with notifications of changes to games.</p>
        <p>
          The information supplied by Rayna is provided for entertainment and informational purposes only. You agree
          that you will only use Rayna, or data/information provided by it, for its intended purposes, and not for
          other commercial ventures without first seeking approval from Rayna.
        </p>
        <p>
          By using Rayna, you may need to interact with other Site users. You
          are solely responsible for any such interaction and agree to do so in a manner that is legal, respectable, and
          consistent with these TOU. Rayna is not responsible for the conduct of any other user who may interact with
          you, regardless of whether or not it is done through the Site or through Rayna's own Discord channel.
        </p>
        <h2>License</h2>
        <p>
          Rayna hereby grants you a revocable and nonexclusive right and license to use Rayna’s
          Site (including any underlying software) and Bot in a manner that is consistent with the other terms in these
          TOU and Rayna’s intended purposes. Rayna reserves the right to terminate this License for any or no
          reason and at any time without notice to you, including, but not limited to, for breach of any term contained
          in these TOU.
        </p>
        <h2>Content Disclaimers</h2>
        <p>
          Rayna does not warrant or guarantee the accuracy, completeness,
          timeliness, merchantability or fitness for a particular purpose of the information and data contained on the
          Site or distributed by the Bot. In no event shall Rayna be liable to you or anyone else for any decision
          made or action taken by you in reliance of any information or data found on the Site.
        </p>
        <h2>Sponsorships</h2>
        <p>
          You also agree to allow us to distribute messages to your Discord server(s) as we deem necessary, without
          limitation. This includes the ability to associate a sponsored message (promoting either Rayna or a third
          party) alongside the message. Any attempt to remove or block sponsored messages from your Discord server(s)
          will result in a temporary or permanent ban of your Discord server(s) from the Rayna service at the
          discretion of Rayna.
        </p>
        <p id="gamer-reach">
          "Gamer Reach" is a term used to describe an estimate of the
          maximum potential viewers of a given message and/or sponsored message. This provides no guarantees as to the
          actual number of views or clicks a given sponsored message will receive.
        </p>
        <p>
          Rayna may, but is not required to, provide a way to opt-out of receiving 3rd party sponsored messages.
          Please <a href="https://discord.gg/W76zRDp">contact us via Discord</a>, if you're unsure how to do this.
        </p>
        <h2>Refunds</h2>
        <p>
          Rayna does not offer any refunds for sponsorships or Premium subscriptions. If you would like to transfer
          your Premium subscription to another Discord server please <a href="https://discord.gg/W76zRDp">contact us via
          Discord</a>.
        </p>
        <h2>Terms of Use Changes</h2>
        <p>
          Rayna reserves the right to modify these TOU at any time without prior notice. You should visit the Site
          from time to time to review the current TOU. By using the Site subsequent to any modification of these TOU,
          you agree to be bound by such modification(s). Rayna will highlight any change to these TOU for 30 days
          after such change(s) is/are made. Rayna does not represent that any of the Site content is completely
          accurate, and therefore any reliance on the Site is done at your own risk.
        </p>
        <h2>Intellectual and Other Property</h2>
        <p>
          Some content on the Site and distributed by the Bot remains property of the 3rd parties where they are
          obtained.
        </p>
        <p>
          Other than the exceptions referenced in these TOU or noted elsewhere, the major exception being UGC
          uploaded/posted by Site users which remains the property of the users, all other content on the Site is the
          property of Rayna including, but not limited to, all marks, logos, names, text, data, documents, messages,
          pictures, images, video, audio, graphics, links, software and its underlying code, domain name, or other
          electronic files (referred hereafter as “Rayna Content”).
        </p>
        <p>
          Certain elements of Rayna, including but not limited to,
          text, graphics, photos, images, video, audio, color selections, organization and layout, are copyright
          protected under United States and international copyright laws and treaty provisions. Any Rayna Content
          protected by intellectual property laws may not be copied, republished, posted, modified, edited, transmitted,
          distributed, used to create derivative works of, or reverse engineered without permission, except that you may
          print out one copy of each Site page solely for noncommercial personal or educational use. No right, title, or
          interest in any Rayna Content is transferred to you as a result of you accessing, downloading, or printing
          such content from the Site. Any use of Rayna Content must display the appropriate copyright, trademark, and
          other proprietary notices.
        </p>
        <p>
          You acknowledge that you have no right, title, or interest in or to Rayna and/or Rayna
          Content.
        </p>
        <p>
          Rayna and the Rayna logo are marks of Rayna. Other marks, names, and logos on the Site are the
          property of their respective owners.
        </p>
        <p>
          There may be other content located on the Site not owned by
          Rayna, and you should respect those property rights as well. All rights not expressly granted herein are
          reserved to Rayna.
        </p>
        <h2>Links to Third Party Sites and other site Interactions</h2>
        <p>
          The Site contains links to external sites not controlled by and/or affiliated with Rayna. If you use these
          links, you will leave the Site. Rayna provides these links to you only as a convenience. Rayna is not
          responsible for the content at the linked sites, including, without limitation, links displayed on such sites.
          You access any linked sites at your own risk.
        </p>
        <p>
          The display of the links are not meant to imply that Rayna guarantees,
          approves, recommends, or endorses the linked sites or any information, content and/or products/services
          available on those linked sites. The links are not meant to indicate any association with Rayna. Rayna
          is not responsible or liable for any linked site nor does Rayna warrant that the linked sites or any goods,
          services, or information on the sites are current, accurate, or error-free. If you access the linked sites,
          you will be subject to the terms of use, privacy, and other policies applicable to those sites. You visit
          those sites at your own risk and should consult the sites’ policies.
        </p>
        <p>
          Rayna may also allow interaction between the Site and other third party sites such as Twitter and other
          social media sites. This may include “Like” buttons or other interactions through third party buttons or
          plugins on the Site that when used, may allow you to share content from the Site or other content with other
          persons on or through the third party sites or elsewhere. Please consult the privacy policies of these third
          party sites before using them to make sure you are comfortable with the level of sharing that will take place
          once you interact with them. It may be very different than how your UGC and other content is shared on the
          Site. Rayna has no control over these third party sites and you use these interaction functions at your own
          risk. Rayna is in no way liable for any harm to you as a result of using one these interaction
          functions.
        </p>
        <p>
          The Site may display third party
          advertisements. If you click on one of these advertisements, you will leave the Site. Rayna is not
          responsible for the content at the linked sites or locations, including, without limitation, links displayed
          on such sites or locations. You access any third party advertisements at your own risk.
        </p>
        <p>
          The display of third party advertisements is not meant to imply that Rayna guarantees, approves,
          recommends, or endorses the products/services advertised, or any other information available on the linked
          sites or locations. The advertisements are not meant to indicate any association with us. If you access the
          linked content through the third party advertisements, you will be subject to the terms of use, privacy, and
          other policies applicable to such content. You visit the linked content at your own risk and should consult
          the applicable policies.
        </p>
        <h2>Other Prohibited Conduct</h2>
        <p>In connection with your access and/or use of the Site or any Site services, you agree not to:</p>
        <ul>
          <li>Violate any federal, state, or local laws or regulations.</li>
          <li>Upload/post anything that imposes an unreasonable or disproportionately large strain on Rayna’s network
            or computer infrastructure.
          </li>
          <li>Engage in any behavior that is designed to hack into or gain unauthorized access to protected areas of the
            Site and/or Rayna’s computers, servers or networks, and/or any computers or systems used by other users
            of the Site.
          </li>
          <li>Upload/post anything that could destroy, damage, or impair any portion of the Site or any computers,
            systems, hardware, or software used by Rayna or other users.
          </li>
          <li>Make unauthorized attempts to modify any information stored on the Site.</li>
          <li>Make attempts to defeat or circumvent security features, or to utilize the Site for any other purpose
            other than its intended purposes.
          </li>
          <li>Discuss, incite, or promote illegal activity.</li>
          <li>Upload/post any unsolicited or unauthorized advertising, promotional materials, spam emails, chain
            letters, pyramid schemes, or any other form of such solicitations.
          </li>
          <li>Use any automated technology such as a robot, spider, or scraper to access, scrape, or data mine the
            Site.
          </li>
          <li>Use the Site to send spam or unsolicited bulk email.</li>
          <li>Provide false or misleading information when signing up for a Site account or otherwise upload/post any
            false or misleading information or content through the Site.
          </li>
        </ul>
        <p>
          The previous list of prohibitions is not exclusive. Rayna reserves the right to terminate your access to
          the Site or any Site services for any reason. Rayna reserves the right to refuse, delete, or edit any UGC
          without cause and without notice for any or no reason including, but not limited to, for any action that
          Rayna determines is inappropriate or disruptive to the Site or to any other user of the Site.
        </p>
        <p>Rayna is under no duty to, and does not represent it will, monitor and/or remove any UGC.</p>
        <p>
          By accepting these TOU, you waive and hold harmless Rayna from any claims resulting from any action taken
          by Rayna during or as a result of Rayna’s investigation and/or from any actions taken as a consequence
          of investigations by either Rayna or law enforcement related to your use of the Site.
        </p>
        <h2>Indemnity</h2>
        <p>
          You agree to indemnify, defend and hold harmless Rayna, including its officers, directors, employees,
          affiliates, agents, licensors, representatives, attorneys, and business partners (“Indemnified Parties”), from
          and against any and all claims, demands, losses, costs, damages, liabilities, judgments, awards, and expenses
          (including attorneys’ fees, costs of defense, and direct, indirect, punitive, special, individual,
          consequential, or exemplary damages) Rayna or any of the Indemnified Parties suffer in relation to, arising
          from, or for the purpose of avoiding, any claim or demand from a third party that relates to your use of the
          Site and/or any Site goods or services, your breach of these TOU, the use of the Site by any person using your
          password, or any violation of an applicable law or regulation by you. Your indemnification obligation shall
          survive the termination of these TOU.
        </p>
        <h2>Disclaimer of Warranties</h2>
        <p>YOUR USE OF THE SITE IS AT YOUR OWN RISK.</p>
        <p>
          RAYNA MAKES NO EXPRESS OR IMPLIED WARRANTIES, REPRESENTATIONS OR ENDORSEMENTS WHATSOEVER
          WITH RESPECT TO THE SITE OR ANY GOODS OR SERVICES OFFERED ON OR THROUGH THE SITE. RAYNA EXPRESSLY DISCLAIMS
          ALL WARRANTIES OF ANY KIND, (EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE), INCLUDING, BUT NOT LIMITED TO,
          IMPLIED WARRANTIES OF MERCHANTABILITY, SECURITY, COMPLETENESS, TIMELINESS, APPROPRIATENESS, ACCURACY, FITNESS
          FOR A PARTICULAR PURPOSE, FREEDOM FROM COMPUTER VIRUSES, TITLE, AND NON-INFRINGEMENT. THE DISCLAIMER OF
          WARRANTIES APPLIES TO THE SITE, ITS CONTENT, AND ANY GOODS OR SERVICES OFFERED ON OR THROUGH THE SITE.
          RAYNA DOES NOT WARRANT THAT THE SITE FUNCTIONS OR CONTENT WILL BE UNINTERRUPTED, TIMELY, OR SECURE.
          RAYNA DOES NOT WARRANT THE ACCURACY OR COMPLETENESS OF THE SITE. RAYNA DOES NOT WARRANT THAT THE SITE
          AND/OR CONTENT WILL BE ERROR-FREE, THAT ANY ERRORS ON THE SITE WILL BE CORRECTED, OR THAT THE SITE/SERVERS ARE
          FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
        </p>
        <p>
          THE SITE AND RELATED CONTENT, INCLUDING ANY GOODS, SERVICES OR INFORMATION PROVIDED
          ON OR THROUGH THE SITE, ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS WITHOUT WARRANTIES OF ANY KIND,
          EITHER EXPRESS OR IMPLIED. YOU ASSUME THE ENTIRE COST OF ALL NECESSARY REPAIRS IN THE EVENT YOU EXPERIENCE ANY
          LOSS OR DAMAGE ARISING FROM THE USE OF THE SITE OR ANY SITE GOODS OR SERVICES. Rayna MAKES NO WARRANTIES
          THAT YOUR USE OF THE SITE WILL NOT INFRINGE THE RIGHTS OF OTHERS AND ASSUMES NO LIABILITY FOR SUCH
          INFRINGEMENT.
        </p>
        <h2>Limitation of Liability</h2>
        <p>
          IN NO EVENT WILL Rayna OR ITS OFFICERS, DIRECTORS, EMPLOYEES, AFFILIATES,
          AGENTS, LICENSORS, REPRESENTATIVES, ATTORNEYS, AND BUSINESS PARTNERS BE LIABLE FOR ANY DAMAGES WHATSOEVER,
          INCLUDING, BUT NOT LIMITED TO, ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, PUNITIVE,
          ACTUAL, OR OTHER INDIRECT DAMAGES, INCLUDING LOSS OF REVENUE OR INCOME, LOST DATA, LOSS OF GOODWILL, PAIN AND
          SUFFERING, EMOTIONAL DISTRESS, OR SIMILAR DAMAGES, EVEN IF RAYNA HAS BEEN ADVISED OF THE POSSIBILITY OF
          SUCH DAMAGES, ARISING OUT OF:
        </p>
        <ol>
          <li>THE USE OR INABILITY TO USE THE SITE OR ANY SITE GOODS OR SERVICES</li>
          <li>ANY TRANSACTION CONDUCTED THROUGH OR FACILITATED BY THE SITE</li>
          <li>ANY CLAIM ATTRIBUTABLE TO ERRORS, OMISSIONS, OR INACCURACIES ON THE SITE</li>
          <li>ANY OTHER MATTER RELATING TO THE SITE OR ANY GOOD OR SERVICE OFFERED ON OR THROUGH THE SITE</li>
        </ol>
        <p>
          IN NO EVENT WILL THE COLLECTIVE LIABILITY OF RAYNA OR ITS OFFICERS, DIRECTORS, EMPLOYEES, AFFILIATES,
          AGENTS, LICENSORS, REPRESENTATIVES, ATTORNEYS, AND BUSINESS PARTNERS TO ANY PARTY, REGARDLESS OF THE TYPE OF
          ACTION WHETHER IN CONTRACT, TORT, OR OTHERWISE, EXCEED THE GREATER OF $100.00 OR THE AMOUNT YOU PAID TO
          RAYNA FOR THE APPLICABLE GOOD OR SERVICE OUT OF WHICH THE LIABILITY AROSE.
        </p>
        <p>
          IF YOU ARE DISSATISFIED WITH THESE TOU, THE SITE, OR ANY GOOD OR SERVICE OFFERED ON OR THROUGH THE SITE, YOUR
          SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.
        </p>
        <p>
          GIVEN THAT SOME STATES DO NOT ALLOW FOR THE EXCLUSION OR LIMITATION OF
          LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU. THE
          LIMITATION OF LIABILITY WILL APPLY TO THE GREATEST EXTENT ALLOWED UNDER THE LAW.
        </p>
        <h4>Geographic Limitation</h4>
        <p>
          Rayna operates the Site from its headquarters in the United States, and the Site and TOU
          are intended only for users within the United States. If you use the Site outside the United States, you are
          responsible for following your applicable local laws and determining, among other things, whether your use of
          the Site violates any of those local laws. By using the Site, you agree and acknowledge that information about
          you, including personally identifiable information, may be transmitted to and stored in the United States.
        </p>
        <h4>Site Privacy Policy</h4>
        <p>
          Your use of the Site and Bot are governed by the <a href="/privacy-policy">Site Privacy Policy</a> which is
          incorporated by reference into these TOU.
        </p>
        <h4>Miscellaneous</h4>
        <p>
          You acknowledge that the opinions and recommendations contained on the Site are not necessarily those of
          Rayna nor endorsed by Rayna. Any reliance on any opinions or recommendations offered on the Site is done
          at your risk. Rayna does not guarantee or promise that any opinions and/or recommendations on the Site are
          accurate or will be helpful to any issue you may have. You agree that Rayna is not liable to you or anyone
          else for any harm that might arise as a result of using and/or implementing in any manner any of the opinions
          or recommendations found on the Site.
        </p>
        <p>
          Any and all disputes relating to these TOU, the Site, and/or any goods
          or services offered on or through the Site, are governed by, and will be interpreted in accordance with, the
          laws of Ohio, without regard to any conflict of laws provisions. You hereby irrevocably and unconditionally
          consent to submit to the exclusive jurisdiction of the courts of Ohio for any litigation arising out of or
          relating to the use of the Site, waive any objection to the venue of any such litigation in Ohio courts, and
          agree not to plead or claim in any Ohio court that such litigation brought therein has been brought in an
          inconvenient forum.
        </p>
        <p>
          If any part of these TOU is determined by a court of competent jurisdiction to be
          invalid or unenforceable, it will not impact any other provision of these TOU, all of which will remain in
          full force and effect.
        </p>
        <p>
          These TOU constitute the entire agreement of the parties with respect to the Site and supersede all prior
          communications, promises and proposals, whether oral, written, or electronic, between you and Rayna, with
          respect to the Site.
        </p>
        <p>
          If you violate any portion of these TOU, Rayna reserves the
          right, without an obligation to do so, to deny you access to the Site and/or remove any UGC you may have
          posted/uploaded on the Site. If Rayna terminates your access to the Site, Rayna may also delete your
          Site account. Rayna has the right to terminate any password-restricted account for any reason.
        </p>
        <p>Rayna’s failure to enforce any portion of these TOU is not a waiver of such portion.</p>
        <p>
          The proprietary rights, disclaimer of warranties, representations made by you, indemnities, and limitations
          of liability shall survive the termination of these TOU.
        </p>
        <p>
          Rayna reserves the right, without notice and reason, to take down or
          terminate the Site or otherwise revoke any and all access granted to you related to the Site. You agree that
          Rayna is not liable to you or any other third party for this action.
        </p>
        <p>
          Rayna does not assume any liability or responsibility for your use of the Internet or the Site including,
          but not limited to, any change your computer or related systems may sustain as a result of accessing the
          Site.
        </p>
        <p>
          You are free to text link to the Site so long as there is nothing deceptive or infringing about the link.
          Rayna may revoke this linking permission at any time and for any reason.
        </p>
        <p>
          Certain software elements of the Site and related Site
          services may be subject to U.S. export laws and controls. As such, no software may be downloaded or exported
          to any country or foreign citizen that is under a U.S. embargo or that would otherwise violate U.S. law or
          regulations.
        </p>
      </div>
  );
};

export default TermsOfServicePage;