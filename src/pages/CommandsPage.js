import React from 'react';
import Accordion from "../components/Accordion";

const CommandsPage = () => {
  return (
      <div className={"commandsPage"}>
        <h2>Commands Page</h2>
        <div className={"commandsSection"}>
          <div className={"commandsCategorySelector"}>
            <div className={"commandsCategorySelectorContent"}>
              <input type={"search"} value={"Search..."}/>
              <div className={"commandSelector"}>
                <ul>
                  <li>Administrative</li>
                  <li>Utility</li>
                  <li>Bot</li>
                  <li>Music</li>
                  <li>Misc</li>
                  <li>Information</li>
                  <li>Image</li>
                  <li>Fun</li>
                </ul>
              </div>
              <div className={"commandsHelp"}>
                <Accordion title={"Command Help"}>
                  <div className="commandsHelpInner">
                    <div className="">
                      <p className="">Default prefix is <code>&#47;&#47;</code>. You can change it at
                        server's dashboard.</p>
                      <p className="">You can use <span className="mention">@Mention</span> as
                        prefix too.</p>
                      <div className="commandsHelpUsages">
                      <span className="">
                        <span className="">
                          <pre>&#47;&#47;help</pre>
                        </span>
                      </span>
                        <span className="">
                        <span className="">
                          <pre>
                          <span className="mention">@Rayna</span> help
                        </pre>
                        </span>
                      </span>
                      </div>
                      <hr className=""/>
                    </div>
                    <h4>Syntax</h4>
                    <ul>
                      <li><code>[]</code> — Optional parameter;</li>
                      <li><code>&lt;&gt;</code> — Required parameter;</li>
                      <li><code>A | B | C</code> — Choose between of these choices. So either A, B or
                        C.
                      </li>
                    </ul>
                  </div>
                </Accordion>
              </div>
            </div>
          </div>
          <table>
            <tr>
              <th>Command</th>
              <th>Description</th>
              <th>Usage</th>
            </tr>
            <tr>
              <td>play</td>
              <td>Rayna will join your voice channel and play the specified song. If she is already playing something, she adds
                specified song to queue.
              </td>
              <td>play &lt;url/name&gt;</td>
            </tr>
            <tr>
              <td>skip</td>
              <td>Skips current playing songs and plays next in queue.</td>
              <td>skip [number of tracks]</td>
            </tr>
            <tr>
              <td>queue</td>
              <td>Shows full playback queue.</td>
              <td>queue [page num]</td>
            </tr>
            <tr>
              <td>history</td>
              <td>Shows the play history.</td>
              <td>history</td>
            </tr>
            <tr>
              <td>join</td>
              <td>Make Rayna join the voice channel.</td>
              <td>join</td>
            </tr>
            <tr>
              <td>leave</td>
              <td>Make Rayna leave the voice channel.</td>
              <td>leave</td>
            </tr>
            <tr>
              <td>lockqueue</td>
              <td>Locks the queue.</td>
              <td>lockqueue</td>
            </tr>
            <tr>
              <td>nowplaying</td>
              <td>Shows the current track being played.</td>
              <td>nowplaying</td>
            </tr>
            <tr>
              <td>pause</td>
              <td>Rayna will pause the music.</td>
              <td>pause [duration]</td>
            </tr>
            <tr>
              <td>repeat</td>
              <td>Repeats the last song played.</td>
              <td>repeat</td>
            </tr>
            <tr>
              <td>restart</td>
              <td>Restarts the current track.</td>
              <td>restart</td>
            </tr>
            <tr>
              <td>shuffle</td>
              <td>Shuffles the queue.</td>
              <td>shuffle</td>
            </tr>
            <tr>
              <td>stop</td>
              <td>Rayna will stop the current track, clear the queue, and leave the voice channel.</td>
              <td>stop</td>
            </tr>
            <tr>
              <td>volume</td>
              <td>Adjust the volume of Rayna.</td>
              <td>volume [number]</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
            <tr>
              <td>placeholderCommand</td>
              <td>Placeholder description.</td>
              <td>Placeholder Command Usage</td>
            </tr>
          </table>
        </div>

      </div>
  )
}

export default CommandsPage;