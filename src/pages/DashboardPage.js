import React from 'react';

const DashboardPage = () => {
  return (
      <div className={"dashboardPage"}>
        <h2>Dashboard Page</h2>
        <div className={"dashboardStats"}>
          <div className={"dashboardStat"}>Members</div>
          <div className={"dashboardStat"}>Channels</div>
          <div className={"dashboardStat"}>Roles</div>
        </div>
      </div>
  );
};

export default DashboardPage;