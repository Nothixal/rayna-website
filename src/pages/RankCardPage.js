import React from 'react';
import userIcon from "../pngs/WolfDiscordImage.png";

const RankCardPage = () => {
  return (
      <div className={"rankCardPage"}>
        <h2>Rank Card Preview</h2>
        <div className={"rankCardWrapper"}>
          <div>
            <img src={userIcon} alt=""/>
          </div>
          <div className={"rankCard"}>
            <div className={"rankCardName"}>
              <span>Nothixal</span>
            </div>
            <div className={"rankCardStats"}>
              <div className={"rankCardCookies"}>1337 Cookies</div>
              <div className={"rankCardStatsInner"}>
                <div>
                  Lv <b>12</b> Rank <b>#1</b>
                </div>
                <div>
                  <b>664</b> / 1420 EXP
                </div>
              </div>
            </div>
            <div className={"rankCardExperienceBar"}>
              <div className={"rankCardExperienceBarOverlay"}/>
            </div>

          </div>
        </div>
        <h2>Color Settings</h2>
        <div className={"colorSettingsWrapper"}>
          <div className={"colorSettings"}>
            <div className={"colorSetting"}>
              <div className={"colorSettingsInner"}>
                <span>Background Color</span>
                <div className={"innerMenuWrapper"}>
                  <input type={"text"} placeholder={"#326526"}/>
                  <div className={"backgroundColorSetting"}/>
                </div>
              </div>
            </div>
            <div className={"colorSetting"}>
              <div className={"colorSettingsInner"}>
                <span>Font Color</span>
                <div className={"innerMenuWrapper"}>
                  <input type={"text"} placeholder={"#326526"}/>
                  <div className={"backgroundColorSetting"}/>
                </div>
              </div>
            </div>
            <div className={"colorSetting"}>
              <div className={"colorSettingsInner"}>
                <span>Progress Background Color</span>
                <div className={"innerMenuWrapper"}>
                  <input type={"text"} placeholder={"#326526"}/>
                  <div className={"backgroundColorSetting"}/>
                </div>
              </div>
            </div>
            <div className={"colorSetting"}>
              <div className={"colorSettingsInner"}>
                <span>Progress Foreground Color</span>
                <div className={"innerMenuWrapper"}>
                  <input type={"text"} placeholder={"#326526"}/>
                  <div className={"backgroundColorSetting"}/>
                </div>
              </div>
            </div>
          </div>
          <div className={"backgroundShadow"}>
            <span>Background Shadow </span>
            <span>&lt;Insert Slider Here&gt;</span>
          </div>
        </div>
        <h2>Custom Background</h2>
        <div className={"customBackgroundWrapper"}>
          <div>Enable Custom Background</div>
          <div className={"fileUpload"}>Drag and drop your picture or Browse...</div>
          <span>Best picture size for rank card is 900 × 200 px.</span>
        </div>
      </div>
  );
};

export default RankCardPage;