import React from 'react';
import {Link} from "react-router-dom";

const HomePage = () => {
  return (
      <div className={"homePage"}>
        <p>This mock up website is a WORK IN PROGRESS. Nothing is concrete and is subject to change at anytime.</p>
        <p>Any Icons, Images and Logos are just temporary placeholders.</p>
        <p>No real functionality has been added.</p>
        <Link to={"/dashboard"}><button>Dashboard</button></Link>
        <Link to={"/premium"}><button>Premium Page</button></Link>
        <Link to={"/billing"}><button>Billing Page</button></Link>
        <Link to={"/search"}><button>Search Page</button></Link>
        <Link to={"/queue"}><button>Queue Page</button></Link>
        <Link to={"/commands"}><button>Commands Page</button></Link>
        <Link to={"/analytics"}><button>Analytics Page</button></Link>
        <Link to={"/assets"}><button>Assets Page</button></Link>
      </div>
  );
};

export default HomePage;