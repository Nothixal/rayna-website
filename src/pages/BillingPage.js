import React from 'react';
import userIcon from "../pngs/WolfDiscordImage.png";

const BillingPage = () => {

  const servers = [
    {
      id: 1,
      name: "Engine Hub",
      image: "https://cdn.discordapp.com/icons/80853743897149440/dda0d8292e7f8a2bfcbf671465eb16f4.png"
    },
    {
      id: 2,
      name: "Discord API",
      image: "https://cdn.discordapp.com/icons/81384788765712384/a363a84e969bcbe1353eb2fdfb2e50e6.png"
    },
    {
      id: 3,
      name: "Pterodactyl Panel",
      image: "https://cdn.discordapp.com/icons/122900397965705216/60ebe9eab01d2e0c7e00a52ece8ece44.png"
    },
    {
      id: 4,
      name: "JDA",
      image: "https://cdn.discordapp.com/icons/125227483518861312/8be466a3cdafc8591fcec4cdbb0eefc0.png"
    },
    {
      id: 5,
      name: "General Testing Server",
      image: "https://cdn.discordapp.com/icons/143004204544294912/503187405103e095584820a697f71aba.png"
    },
    {
      id: 6,
      name: "Poniverse",
      image: "https://cdn.discordapp.com/icons/204445437564813312/a_e2d48b40a4eb7512af5f2655c0d504e6.png"
    },
    {
      id: 7,
      name: "LuckPerms",
      image: "https://cdn.discordapp.com/icons/241667244927483904/a5e13b12fe00231cb2ced95bfb497fc2.png"
    },
    {
      id: 8,
      name: "Rainmeter",
      image: "https://cdn.discordapp.com/icons/148103787259756544/a_29716a168727cb1034f4968f4d51d088.png"
    },
    {
      id: 9,
      name: "PaperMC",
      image: "https://cdn.discordapp.com/icons/289587909051416579/a_eac276de7e9e5324078d0026b067be7d.png"
    },
    {
      id: 10,
      name: "Sx4 | Support",
      image: "https://cdn.discordapp.com/icons/330399610273136641/f3715fd3694fe29b40420ac5df18a311.png"
    },
    {
      id: 11,
      name: "LBRY",
      image: "https://cdn.discordapp.com/icons/362322208485277697/4cec9b98e33c4689d7b0845ff51ae9b0.png"
    },
    {
      id: 12,
      name: "WebDevs",
      image: "https://cdn.discordapp.com/icons/435107908175527946/505bed77946c3d10eff308de0bb47bba.png"
    },
    {
      id: 13,
      name: "Shiro",
      image: "https://cdn.discordapp.com/icons/493015512960204802/1a13b87b1966721a201015dcac032930.png"
    },
    {
      id: 14,
      name: "Sweet Bell Day",
      image: "https://cdn.discordapp.com/icons/337402652843311106/d2af175f80273cfa45393641227ed28c.png"
    },
    {
      id: 15,
      name: "Tekk",
      image: "https://cdn.discordapp.com/icons/328389084261122049/a_ea964c1fb141575a2527febcbbaf9d08.png"
    }
  ]

  return (
      <div className={"billingPage"}>
        <div className={"billingHeader"}>
          <img src={userIcon} alt="User Icon"/>
          <h4>Nothixal#4875 you are currently using the Premium Plan</h4>
          <button>
            Start Free Trial
          </button>
        </div>
        <div>
          <h2>Email Address</h2>
          <div className={"billingEmail"}>
            <form>
              <input type={"text"} value={"example@example.com"} disabled/>
            </form>
          </div>
        </div>
        <div className={"billingOverview"}>
          <h2>Billing Information</h2>
          <div className={"billingOverviewCard"}>
            <div className={"planOverview"}>
              <div>
                {/*<h3>Rayna Premium</h3>*/}
                {/*<span>Premium Plan</span>*/}
                <h3>Premium Plan</h3>
                <span>Thank you for being a premium member!</span>
              </div>
              <div className={"thankYou"}>
                <b>$4.99</b><span>/Monthly</span>
              </div>
            </div>
            <hr/>
            {/*<span>Thanks for being a premium member.</span>*/}
            <ul>
              <li>
                <p>Next billing date: June 15, 2091</p>
                <button>
                  <span>DEACTIVATE</span>
                </button>
              </li>
              <li>
                <p>Billed with Visa **** 6789</p>
                <button>
                  <span>Edit</span>
                </button>
              </li>
              <li>
                <p>Backup payment method</p>
                <button>
                  <span>Edit</span>
                </button>
              </li>
            </ul>
          </div>
          <h2>Billing History</h2>
          <div>

          </div>
        </div>
        <div className={"premiumServerSelector"}>
          <div className={"serverSelectorTitle"}>
            <h2>Server Selector</h2>
            <span className={"selectedServerCount"}>
              <span className={"selectedServersUsed"}>00</span>
              /05
            </span>
          </div>
          <div className={"serverSelectorChoices"}>
            {servers.map((server, id) => (
                <div className={"server"}>
                  <img src={server.image} alt={server.name} key={id}/>
                  <span>{server.name}</span>
                </div>
            ))}
            {/*<img src="https://cdn.discordapp.com/icons/80853743897149440/dda0d8292e7f8a2bfcbf671465eb16f4.png" alt="server"/>*/}
          </div>
        </div>
      </div>
  );
};

export default BillingPage;