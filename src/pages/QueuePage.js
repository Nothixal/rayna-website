import React from 'react';
import userIcon from "../pngs/WolfDiscordImage.png";
import {ReactComponent as FavoriteIcon} from "../svgs/likeFilled.svg";
import {ReactComponent as LikeIcon} from "../svgs/like.svg";
import {ReactComponent as OptionsIcon} from "../svgs/dropdownDots.svg";
import {ReactComponent as PlayIcon} from "../svgs/playAlt.svg";
import {ReactComponent as TrashIcon} from "../svgs/trash.svg";

// const initialData = {
//   tasks: {
//     'task-1': { id: 'task-1', content: 'Take out the garbage'},
//     'task-2': { id: 'task-2', content: 'Watch my favorite show'},
//     'task-3': { id: 'task-3', content: 'Charge my phone'},
//     'task-4': { id: 'task-4', content: 'Cook dinner'},
//     'task-5': { id: 'task-5', content: 'Update this list'},
//   },
//   columns: {
//     'column-1': {
//       id: 'column-1',
//       title: 'To do',
//       taskIds: ['task-1', 'task-2', 'task-3', 'task-4', 'task-5']
//     }
//   },
//   columnOrder: ['column-1']
// }

const QueuePage = () => {

  return (
      <div className={"queuePage"}>
        <div className={"queueHeaderText"}>
          <h1>Current Queue</h1>
          <span>521 Songs, 21h 38m 52s</span>
        </div>

        <h2>ORIGINAL IDEA - Queue with flexbox layout.</h2>

        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d000048518d5d85cb1d6adcf3533907a2" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <p>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</p>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackAlbum"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDuration"}>
              <div>1:02</div>
            </div>
            <div className={"trackControls"}>
              <button>
                <FavoriteIcon/>
              </button>

              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d00004851dfdcd80e0cfc273bb62ce729" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <p>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</p>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackAlbum"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDuration"}>
              <div>1:02</div>
            </div>
            <div className={"trackControls"}>
              <button>
                <FavoriteIcon/>
              </button>

              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d00004851d8601e15fa1b4351fe1fc6ae" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <p>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</p>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackAlbum"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDuration"}>
              <div>1:02</div>
            </div>
            <div className={"trackControls"}>
              <button>
                <FavoriteIcon/>
              </button>

              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d0000485156b357f7ee19019fea21f95f" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <p>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</p>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackAlbum"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDuration"}>
              <div>1:02</div>
            </div>
            <div className={"trackControls"}>
              <button>
                <FavoriteIcon/>
              </button>

              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d000048512ebd3c8519497dc562f8f9aa" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <p>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</p>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackAlbum"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDuration"}>
              <div>1:02</div>
            </div>
            <div className={"trackControls"}>
              <button>
                <FavoriteIcon/>
              </button>

              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <h2>REV 1 - Queue with flexbox layout.</h2>

        {/*https://i.ytimg.com/vi/-M4LS28C16Q/mqdefault.jpg*/}
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d00004851846f0421bf1d4d4d1b9c548e" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <a href={"#2"}>2019</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackControls"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"nowPlayingWrapper"}>
          <div className={"nowPlaying"}>
            {/*<div className={"trackNumber"}>1</div>*/}
            <div className={"trackInfo"}>
              <div className={"trackArtwork"}>
                <img src="https://i.scdn.co/image/ab67616d0000485106c95838d734f5c0fe3a6a7f" alt="Artwork"/>
              </div>
              <div className={"trackContent"}>
                <a href={"#2"}>Suicidal Thoughts</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <span>ACutiePie (Nothixal#4875)</span>
              </div>
            </div>
            <div className={"trackControls"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <h2>REV 2 - Queue with grid layout.</h2>

        <div className={"gridQueueHeader"}>
          <span>Title</span>
          <span>Platform</span>
          <span>Duration</span>
          <span>Controls</span>
        </div>

        <div className={"gridTestingWrapper first"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/mNgeWabM-us/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=mNgeWabM-us"} target={"_blank"} rel={"noreferrer"}>Smash Into Pieces - Forever Alone (Official Lyric Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>3:14</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/2mrf2ogIKbo/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=2mrf2ogIKbo"} target={"_blank"} rel={"noreferrer"}>Win It All - Derivakat [MCC original song]</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>2:40</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/w4QcB583F9o/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=w4QcB583F9o"} target={"_blank"} rel={"noreferrer"}>【Music】The Prince of Asliyah</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>3:46</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid youtube"}>
                <img src="https://i.ytimg.com/vi/qFfybn_W8Ak/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLB06lTZDK7b0Ru0cDsZsgL4DmfagA" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=qFfybn_W8Ak"} target={"_blank"} rel={"noreferrer"}>Carpenter Brut - Roller Mobster</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>3:36</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/3icALOQJnWg/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=3icALOQJnWg"} target={"_blank"} rel={"noreferrer"}>Solence - Direction (Official Lyric Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>4:37</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/yuySSmW5cfo/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=yuySSmW5cfo"} target={"_blank"} rel={"noreferrer"}>C418 - Flake (Minecraft Volume Beta)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Youtube</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>2:49</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.scdn.co/image/ab67616d0000485184bb939e5abfa8560da4a6c5" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid soundcloud"}>
                <a href={"#2"}>Placeholder Soundcloud Track</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Soundcloud</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>???</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i1.sndcdn.com/artworks-Nj4aOwa3dOJB-0-large.png" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid soundcloud"}>
                <a href={"https://soundcloud.com/au5/was-it-you-mazare-remix"} target={"_blank"} rel={"noreferrer"}>Was It You (Mazare Remix)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Soundcloud</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>4:33</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapper"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://f4.bcbits.com/img/a2385887365_9.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid bandcamp"}>
                <a href={"https://vyletpony.bandcamp.com/track/strange-world-ft-4everfreebrony-namii-sylver-stripe"} target={"_blank"} rel={"noreferrer"}>
                  Strange World (ft. 4everfreebrony, Namii, & Sylver Stripe)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackAlbumGrid"}>
              <a href={"/queue"}>Bandcamp</a>
            </div>
            <div className={"trackDurationGrid"}>
              <div>5:58</div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <h2>REV 3 - Queue with grid layout.</h2>

        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/-M4LS28C16Q/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=-M4LS28C16Q"} target={"_blank"} rel={"noreferrer"}>Solence - In The Dark [OFFICIAL AUDIO]</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/hwy0636eYsA/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=hwy0636eYsA"} target={"_blank"} rel={"noreferrer"}>Solence - Animal In Me (Official Lyric Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/cLlimH3t4AU/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=cLlimH3t4AU"} target={"_blank"} rel={"noreferrer"}>Solence - 4 Good Reasons (Official Lyric Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/lDsNgdleih8/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=lDsNgdleih8"} target={"_blank"} rel={"noreferrer"}>Solence - Good F**king Music (Official Music Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.ytimg.com/vi/8eMrsYovpDs/mqdefault.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid youtube"}>
                <a href={"https://www.youtube.com/watch?v=8eMrsYovpDs"} target={"_blank"} rel={"noreferrer"}>Solence - DEAFENING (Official Music Video)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i.scdn.co/image/ab67616d000048515d108987251edf52a4d44a44" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid soundcloud"}>
                <a href={"#2"}>Placeholder Soundcloud Track</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://i1.sndcdn.com/artworks-xP6AMoT6ZePM5k53-MjY6DQ-large.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid soundcloud"}>
                <a href={"https://soundcloud.com/scratonmusicofficial/loud-is-not-enough"} target={"_blank"} rel={"noreferrer"}>Loud Is Not Enough</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>
        <div className={"gridTestingWrapperModified"}>
          <div className={"gridTesting"}>
            {/*<div>1</div>*/}
            <div className={"trackInfoGrid"}>
              <div className={"trackArtworkGrid"}>
                <img src="https://f4.bcbits.com/img/a1671366151_9.jpg" alt="Artwork"/>
              </div>
              <div className={"trackContentGrid bandcamp"}>
                <a href={"https://crystarium.bandcamp.com/track/rockstar-crystarium-remix"} target={"_blank"} rel={"noreferrer"}>
                  Post Malone - Rockstar (Crystarium Remix)</a>
                {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                <div className={"trackSubContentGrid"}>
                  <img src={userIcon} alt="UserIcon"/>
                  <span>ACutiePie (Nothixal#4875)</span>
                </div>
              </div>
            </div>
            <div className={"trackControlsGrid"}>
              <button>
                <PlayIcon/>
              </button>
              <button>
                <TrashIcon/>
              </button>
              <button>
                <LikeIcon/>
              </button>
              <button>
                <OptionsIcon/>
              </button>
            </div>
          </div>
        </div>

        <h2>REV 4 - Queue with grid layout using lists.</h2>

        <ul>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.ytimg.com/vi/ACNgFW50EbU/mqdefault.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>The Ringer</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.ytimg.com/vi/g4tCJtfCV8Y/mqdefault.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid youtube"}>
                    <a href={"https://www.youtube.com/watch?v=g4tCJtfCV8Y"} target={"_blank"} rel={"noreferrer"}>NF - LAYERS (Audio)</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.ytimg.com/vi/okdbKcprsgY/mqdefault.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid youtube"}>
                    <a href={"https://www.youtube.com/watch?v=okdbKcprsgY"} target={"_blank"} rel={"noreferrer"}>Tech N9ne - Strangeulation Cypher</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.ytimg.com/vi/-T7LhhK_r_o/mqdefault.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid youtube"}>
                    <a href={"https://www.youtube.com/watch?v=-T7LhhK_r_o"} target={"_blank"} rel={"noreferrer"}>Hollywood Undead - Heart of a Champion [Lyrics Video]</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.ytimg.com/vi/5Di20x6vVVU/mqdefault.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid youtube"}>
                    <a href={"https://www.youtube.com/watch?v=5Di20x6vVVU"} target={"_blank"} rel={"noreferrer"}>Juice WRLD ft. Marshmello - Come & Go (Official Audio)</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i.scdn.co/image/ab67616d000048511f675e7b8bae408653346dd9" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid soundcloud"}>
                    <a href={"#2"}>Placeholder Soundcloud Track</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://i1.sndcdn.com/artworks-UI7aUD3pRq0kzkjC-6uABiA-large.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid soundcloud"}>
                    <a href={"https://soundcloud.com/saintpitre/target"} target={"_blank"} rel={"noreferrer"}>Target (prod. Suni Vega)</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"gridTestingWrapperModified"}>
              <div className={"gridTesting"}>
                {/*<div>1</div>*/}
                <div className={"trackInfoGrid"}>
                  <div className={"trackArtworkGrid"}>
                    <img src="https://f4.bcbits.com/img/a1941217478_9.jpg" alt="Artwork"/>
                  </div>
                  <div className={"trackContentGrid bandcamp"}>
                    <a href={"https://sleepysound.bandcamp.com/track/frozen-in-the-fallout"} target={"_blank"} rel={"noreferrer"}>
                      Frozen In The Fallout</a>
                    {/*The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands*/}
                    <div className={"trackSubContentGrid"}>
                      <img src={userIcon} alt="UserIcon"/>
                      <span>ACutiePie (Nothixal#4875)</span>
                    </div>
                  </div>
                </div>
                <div className={"trackControlsGrid"}>
                  <button>
                    <PlayIcon/>
                  </button>
                  <button>
                    <TrashIcon/>
                  </button>
                  <button>
                    <LikeIcon/>
                  </button>
                  <button>
                    <OptionsIcon/>
                  </button>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
  )
}

export default QueuePage;