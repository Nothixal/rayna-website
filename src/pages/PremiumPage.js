import React, {useState} from 'react';
import userIcon from "../pngs/WolfDiscordImage.png";
import {ReactComponent as VolumeIcon} from "../svgs/rythm/volume.svg";
import {ReactComponent as LongerSongsIcon} from "../svgs/rythm/fastforward.svg";
import {ReactComponent as BetterAudioIcon} from "../svgs/rythm/microphone.svg";
import {ReactComponent as AudioEffectsIcon} from "../svgs/rythm/lightning.svg";
import {ReactComponent as AutoplayIcon} from "../svgs/rythm/autoplay.svg";
import {ReactComponent as AlwaysPlayingIcon} from "../svgs/rythm/clock.svg";
import {ReactComponent as ExtraBotsIcon} from "../svgs/rythm/extraBots.svg";
import {ReactComponent as BadgeIcon} from "../svgs/rythm/badge.svg";
import {ReactComponent as SupportIcon} from "../svgs/rythm/support.svg";
import Accordion from "../components/Accordion";
import Switch from "react-switch";
import StandardPlans from "../components/premium/StandardPlans";
import CustomPlans from "../components/premium/CustomPlans";

const PremiumPage = () => {
  const [checked, setChecked] = useState(false);
  const handleChange = nextChecked => {
    setChecked(nextChecked);
  };

  return (
      <div className={"premiumPage"}>
        <div className={"premiumHeader"}>
          <img src={userIcon} alt="User Icon"/>
          <h4>Nothixal#4875 you are currently using the Free Plan</h4>
          <button>
            Start Free Trial
          </button>
        </div>
        <div className={"premiumPlans"}>
          <div className="sectionHeader">
            <div className={"sectionHeadingText"}>
              <h2>Plans & Pricing</h2>
              <span>Here's a look at what we include in our default plans.</span>
            </div>
            <div className={"pricingSwitcher"}>
              <div className={"planSwitcher"}>
                <span>Standard</span>
                <Switch
                    checked={checked}
                    onChange={handleChange}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    onColor="#86d3ff"
                    onHandleColor="#2693e6"
                >
                </Switch>
                <span>Custom</span>
              </div>
            </div>
          </div>

          <div>
            { checked ? <CustomPricingOptions/> : <StandardPricingOptions/> }
          </div>
          {/*<div className={"extraFeatures"}>*/}
          {/*  <span>Extra Features Section</span>*/}
          {/*  <ul>*/}
          {/*    <li>*/}
          {/*      <div className="additionalFeatures" onClick={() => setCheckboxValue(!checkboxVal)}>*/}
          {/*        <div className={"extraFeature"}>*/}
          {/*          <div className={"extraFeatureTitle"}>Really Cool Feature</div>*/}
          {/*          <div className={"extraFeatureDescription"}>This is some feature that will almost guarantee a better experience.</div>*/}
          {/*        </div>*/}
          {/*        <div className={"featurePricing"}>*/}
          {/*          <div><span className={"featurePrice"}>$4.99</span>/mo</div>*/}
          {/*          <div>*/}
          {/*            <form>*/}
          {/*              <input*/}
          {/*                  name={"added"}*/}
          {/*                  type={"checkbox"}*/}
          {/*                  onChange={() => setCheckboxValue(!checkboxVal)}*/}
          {/*              />*/}
          {/*              <label>Add</label>*/}
          {/*            </form>*/}
          {/*          </div>*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*    </li>*/}
          {/*    <li>*/}
          {/*      <div className="additionalFeatures">*/}
          {/*        <div className={"extraFeature"}>*/}
          {/*          <div className={"extraFeatureTitle"}>Total Servers</div>*/}
          {/*          <div className={"extraFeatureDescription"}>How many servers do you want to have premium?</div>*/}
          {/*        </div>*/}
          {/*        <div className={"featurePricing"}>*/}
          {/*          <div><span className={"featurePrice"}>${price}</span>/mo</div>*/}
          {/*          <button onClick={decrementNumber}>-</button>*/}
          {/*          <div>*/}
          {/*            {numberOfServers}*/}
          {/*          </div>*/}
          {/*          <button onClick={incrementNumber}>+</button>*/}
          {/*        </div>*/}
          {/*      </div>*/}
          {/*    </li>*/}
          {/*  </ul>*/}


          {/*  <div className={"paymentButton"}>*/}
          {/*    <button>Continue to Payment</button>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </div>
        <div className={"premiumFeatures"}>
          <div className="sectionHeader">
            <div className={"sectionHeadingText"}>
              <h2>What comes with Premium?</h2>
              <span>Here's the full list of things you get with Rayna Premium.</span>
            </div>
          </div>
          <div className={"features"}>
            <div className="feature">
              <div className="icon">
                <VolumeIcon/>
              </div>
              <div className="info">
                <h3>Volume Control</h3>
                <p>Change the volume for everyone in the voice channel all at once.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <LongerSongsIcon/>
              </div>
              <div className="info">
                <h3>Longer Songs</h3>
                <p>Say goodbye to song length limits when you have premium!</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <BetterAudioIcon/>
              </div>
              <div className="info">
                <h3>Better Audio Quality</h3>
                <p>Enjoy twice the bots normal audio quality, 2x the bitrate.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <AudioEffectsIcon/>
              </div>
              <div className="info">
                <h3>Audio Effects</h3>
                <p>Allows you to speed up, slow down, nightcore and bass boost songs.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <AutoplayIcon/>
              </div>
              <div className="info">
                <h3>Autoplay</h3>
                <p>When your queue ends, Rayna will automatically play something else.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <AlwaysPlayingIcon/>
              </div>
              <div className="info">
                <h3>Always Playing</h3>
                <p>The bot stays in the voice channel 24/7, ready to play when you join.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <ExtraBotsIcon/>
              </div>
              <div className="info">
                <h3>Extra Bots</h3>
                <p>To support multiple voice channels, get multiple Premium Rayna's.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <BadgeIcon/>
              </div>
              <div className="info">
                <h3>Dashboard Badge</h3>
                <p>Show off your premium with a badge in our web player.</p>
              </div>
            </div>
            <div className="feature">
              <div className="icon">
                <SupportIcon/>
              </div>
              <div className="info">
                <h3>Priority Support</h3>
                <p>When support is busy, get your tickets prioritized to the top.</p>
              </div>
            </div>
          </div>
          <span className={"featuresEnd"}>And much much more!</span>
        </div>
        <div className={"premiumFAQ"}>
          <div className="sectionHeader">
            <div className={"sectionHeadingText"}>
              <h2>Frequently Asked Questions</h2>
              <span>You may have asked them, we're answering them.</span>
            </div>
          </div>
          <div className={"faq"}>
            <ul>
              <li>
                <Accordion title={"What is Rayna premium?"}>
                  Rayna Premium is a subscription you can get to support the development of the bot. It is not mandatory to use Rayna but allows you to unlock exclusive features if you get it.
                </Accordion>
              </li>
              <li>
                <Accordion title={"Can I get a refund if I don't like it?"}>
                  We want everyone to have a great experience with Rayna Premium. If you're not satisfied during your first week, please contact us and we will issue you a full refund
                </Accordion>
              </li>
              <li>
                <Accordion title={"How many Discord servers is my subscription valid for?"}>
                  A subscription is valid for a single Discord server. If you wish to get premium for multiple servers, you need a subscription for each server.
                </Accordion>
              </li>
              <li>
                <Accordion title={"Can I transfer my premium subscription to another server?\n"}>
                  Yes. Once you are subscribed, you'll be able to transfer your subscription to another server of yours from this page.
                </Accordion>
              </li>
              <li>
                <Accordion title={"Can I stop my subscription anytime?"}>
                  Yes. You can stop your subscription at any time by going to your billing tab. Your subscription will still be valid until the end of the period you have paid for, but you will not be charged anymore.
                </Accordion>
              </li>
              <li>
                <Accordion title={"What payment methods are supported?"}>
                  Currently, we support only credit cards, debit cards, and PayPal. Please note that if your PayPal account is not verified, the transaction might not work. You can contact us if you would like to pay with another payment method.
                </Accordion>
              </li>
              <li><Accordion title={"Can I pay in another currency?"}>
                The only currency available is the one displayed. However, you can still pay in your own currency because most payment processors will do the conversion for you for a minor fee (usually a few cents). PayPal does not take fees for conversion.
              </Accordion></li>
              <li>
                <Accordion title={"Can I get an invoice for my payment?"}>
                  Yes! After subscribing you can see and download all your invoices in your "Billing" tab (located in the dropdown of your profile, in the top right of the page). They should be valid for almost all purposes, including business accounting. If you need any help with an invoice, please contact us.
                </Accordion>
              </li>
              <li>
                <Accordion title={"What is the money used for?"}>
                  Rayna is a real company run by real people. Premium is what keeps it alive. We use the money to pay salaries to our developers and to cover hosting costs and taxes.
                </Accordion>
              </li>
              <li>
                <Accordion title={"Is my payment secure?"}>
                  Yes. We never have access to your debit/credit card information. Payments are handled through Stripe, a payment processor trusted by millions of companies worldwide and protected with state-of-the-art technologies. We also never have access to your PayPal account info and your transactions are protected by PayPal's Purchase Protection for Buyers.
                </Accordion>
              </li>
            </ul>
          </div>
        </div>
      </div>
  );
};

const StandardPricingOptions = () => {
  return(
      <StandardPlans/>
  );
}

const CustomPricingOptions = () => {
  return(
      <CustomPlans/>
  );
}

export default PremiumPage;