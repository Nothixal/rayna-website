import React from 'react';
import {ReactComponent as AddToQueueIcon} from "../svgs/rythm/addToQueue.svg";
import {ReactComponent as PlayNextIcon} from "../svgs/rythm/playNext.svg";
import {ReactComponent as PlayNowIcon} from "../svgs/playAlt.svg";

const SearchPage = () => {
  return (
      <div className={"searchPage"}>
        <h2>Search Page</h2>

        <input type={"text"} placeholder={"Search..."}/>

        <h3>Search results for "eminem the ringer"</h3>

        <ul>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/tT150Zl0Ay0/hqdefault.jpg?sqp=-oaymwEcCOADEI4CSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAhY_Ct_3hVG8IgGYtw1tI4j_8mvg"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>Eminem
                      - The Ringer (Lyrics)</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>Shady Media</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>5:42</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/c2AhsySa-8E/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDC_b1Wyb045aI-iz_prh_W7AGlTw"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>Eminem
                      - The Ringer (Official Audio) HD</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>ΞMINΞM KΛMIKΛZΞ</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>5:39</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/S2ujuC9VuSM/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLBrG2MXddc5bh7QXrm7bin84jDDBw"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>Eminem
                      - The Ringer [ Music Video ]</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>CHEROKEE</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>5:43</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/0Ea-vXKUf80/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAqoEzwbtzArtZ5Vk2JZraE56RS_Q"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>Eminem
                      - The Ringer (REACTION!!!)</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>Lost In Vegas</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>15:00</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/XCdD1WD7EDs/hqdefault.jpg?sqp=-oaymwEcCOADEI4CSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLAIw_V4xGTBaQRlx30mlZWvWNtcOA"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>EMINEM
                      - THE RINGER - BRUH WHY YALL DO THIS TO ME?</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>Stevie Knight</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>34:50</span>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div className={"searchResultWrapper"}>
              <div className={"searchResult"}>
                {/*<div>1</div>*/}
                <div className={"resultInfo"}>
                  <div className={"resultArtwork"}>
                    <img
                        src="https://i.ytimg.com/vi/a5b6hso6wsU/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDrNNVR63UclpXlKqeZh6BtBURXlg"
                        alt="Artwork"/>
                  </div>
                  <div className={"resultContent youtube"}>
                    <a href={"https://www.youtube.com/watch?v=ACNgFW50EbU"} target={"_blank"} rel={"noreferrer"}>A Whole World - Derivakat feat. krustydavid</a>
                    <div className={"resultSubContent"}>
                      {/*<img src={userIcon} alt="UserIcon"/>*/}
                      <span>Derivakat</span>
                    </div>
                  </div>
                </div>
                <div className={"resultControls"}>
                  <button>
                    <AddToQueueIcon/>
                  </button>
                  <button>
                    <PlayNextIcon/>
                  </button>
                  <button>
                    <PlayNowIcon/>
                  </button>
                </div>
                <div className={"resultDuration"}>
                  <span>34:50</span>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
  );
};

export default SearchPage;