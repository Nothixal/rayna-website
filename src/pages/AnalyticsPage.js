import React from 'react';
import {ReactComponent as TempIcon} from "../svgs/support.svg";
import guildIcon from "../pngs/guildIcon.png";

const AnalyticsPage = () => {
  return (
      <div className={"analyticsPage"}>
        <div className={"analyticsHeader"}>
          <img src={guildIcon} alt="User Icon"/>
          <h4>Analytics for General Testing Server</h4>
          <button>
            Start Free Trial
          </button>
        </div>
        <div className={"analyticsContent"}>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>Messages Sent</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>1.45M</h3>
                  <p>+35%</p>
                </div>
                <span>+11.8% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>Total Member Count</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>17.35K</h3>
                  <p>+8.3%</p>
                </div>
                <span>+9.61% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>New Members</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>12</h3>
                  <p>- 2.1%</p>
                </div>
                <span>+3.27% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>Active Users Yesterday</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>31</h3>
                  <p>+3.5%</p>
                </div>
                <span>+11.8% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>Reactions Added</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>17</h3>
                  <p>+1.3%</p>
                </div>
                <span>+5.61% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
          <div className={"analyticsCard"}>
            <div className={"analyticsCardTitle"}>
              <span>Total Voice Time</span>
            </div>
            <div className={"analyticsCardContent"}>
              <div className={"analyticsCardContentStatus"}>
                <div className={"analyticsCardContentStatusHeader"}>
                  <h3>2,039 Minutes</h3>
                  <p>-2.1%</p>
                </div>
                <span>+2.27% Since last month</span>
              </div>
              <div className={"analyticsCardContentImage"}>
                <TempIcon/>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default AnalyticsPage;