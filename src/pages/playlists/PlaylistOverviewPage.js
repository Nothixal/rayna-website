import React from 'react';
import Playlists from "../../components/Playlists";

const PlaylistOverviewPage = () => {
  const dataCategories = [
    {
      id: 1,
      name: "Focus",
      tagline: "Music to help you concentrate"
    },
    {
      id: 2,
      name: "Mood",
      tagline: "Playlists to match your mood"
    },
    {
      id: 3,
      name: "Soundtrack your home",
      tagline: ""
    },
    {
      id: 4,
      name: "Kick back this Sunday...",
      tagline: ""
    },
  ]

  return (
      <div className={"mainInner"}>
        <h1>Playlist Page</h1>

        {dataCategories.map((category, id) => (
            <div className="cardsWrap" key={id}>
              <h2>{category.name}</h2>
              {/*<span className={"seeAll"}>SEE ALL</span>*/}
              <p className={"subText"}>Music to help you concentrate.</p>

              <Playlists categoryId={category.id} />
            </div>
        ))}

      </div>
  )
};

export default PlaylistOverviewPage;