import React from 'react'
//import {useParams} from 'react-router-dom'
import userIcon from "../../pngs/WolfDiscordImage.png";
import {ReactComponent as DropdownIcon} from "../../svgs/dropdownAlt.svg";
import {ReactComponent as DurationIcon} from "../../svgs/spotify/duration.svg";
import {ReactComponent as LikeIcon} from "../../svgs/like.svg";
import {ReactComponent as OptionsIcon} from "../../svgs/dropdownDots.svg";
import {ReactComponent as PlayIcon} from "../../svgs/play.svg";
import {ReactComponent as TrashIcon} from "../../svgs/trash.svg";

const PlaylistPage = () => {
  //let {id} = useParams()

  return (
      <div className={"playlistPage"}>
        <div className={"playlistHeader"}>
          <div className={"playlistHeaderImage"}>
            <img
                src="https://images.unsplash.com/photo-1615763282652-54418ea841fc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80"
                alt="Artwork"/>
          </div>
          <div className={"playlistHeaderInfo"}>
            <h3>Personal Playlist</h3>
            <h1>Playlist Name</h1>
            <div className={"playlistHeaderSubInfo"}>
              <img src={userIcon} alt="UserIcon"/>
              <a href="#a">Nothixal#4875</a>
              <span>2,537,215 likes</span>
              <span>102 Songs, 12 hr 21 min</span>
            </div>
          </div>
        </div>
        <div className={"playlistControls"}>
          <div className={"playlistControlsLeft"}>
            <button className={"playButton"}><PlayIcon/></button>
            <button><LikeIcon/></button>
            <button><OptionsIcon/></button>
          </div>
          <div className={"playlistControlsRight"}>
            <input type={"search"} value={"Search..."}/>

            <div className={"playlistSortDropdown"}>
              <button className={"playlistSortButton"}>
                <span className={"playlistSortButtonText"}>Custom Sort</span>
                <div className={"playlistSortCaretWrapper"}>
                  <DropdownIcon/>
                </div>
              </button>
              <div className={"playlistSortDropdownContent"}>
                <ul>
                  <li>

                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className={"playlistContent"}>
          <table>
            <thead>
              <th>#</th>
              <th>Title</th>
              <th>Platform</th>
              <th>Date Added</th>
              <th>
                <DurationIcon/>
              </th>
              <th>Controls</th>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Applying Concurrency and Multi-threading to Common Java Patterns</td>
                <td>Youtube</td>
                <td>Mar 3, 2021</td>
                <td>3:02</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>3 Shots (feat. Bobby Shmurda, Chinx & Rowdy Rebel)</td>
                <td>Youtube</td>
                <td>Mar 4, 2021</td>
                <td>4:55</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td>The Black Hawk War, or, How to Demolish an Entire Civilization and Still Feel Good About Yourself in the Morning, or, We Apologize for the Inconvenience but You’re Going to Have to Leave Now, or, ‘I Have Fought the Big Knives and Will Continue to Fight Them Until They Are Off Our Lands</td>
                <td>Youtube</td>
                <td>Mar 7, 2021</td>
                <td>7:38</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>4</td>
                <td>Some Track</td>
                <td>Soundcloud</td>
                <td>Apr 2, 2021</td>
                <td>2:43</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>5</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Apr 8, 2021</td>
                <td>3:34</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>6</td>
                <td>Some Track</td>
                <td>Soundcloud</td>
                <td>Apr 15, 2021</td>
                <td>2:56</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>7</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Jun 17, 2021</td>
                <td>1:47</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>8</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Aug 30, 2021</td>
                <td>4:22</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>9</td>
                <td>Some Track</td>
                <td>Bandcamp</td>
                <td>Oct 11, 2021</td>
                <td>4:17</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>10</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>11</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>12</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>13</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>14</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>15</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>16</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>17</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>18</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>19</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>20</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>21</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>22</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>23</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>24</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>25</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>26</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>27</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>28</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>29</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>30</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>31</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>32</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>33</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>34</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>35</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>36</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>37</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>38</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>39</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
              <tr>
                <td>40</td>
                <td>Some Track</td>
                <td>Youtube</td>
                <td>Dec 12, 2021</td>
                <td>3:01</td>
                <td>
                  <div className={"trackOptions"}>
                    <button>
                      <LikeIcon/>
                    </button>
                    <button>
                      <TrashIcon/>
                    </button>
                    <button>
                      <OptionsIcon/>
                    </button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>


        {/*<div className={"mainInner"}>*/}
        {/*  <div className={"playlistPageInfo"}>*/}
        {/*    <div className={"playlistPageImage"}>*/}
        {/*      <img*/}
        {/*          src={"https://images.unsplash.com/photo-1615763282652-54418ea841fc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80"}*/}
        {/*          alt={"Artwork"}/>*/}
        {/*    </div>*/}
        {/*    <h1>Title</h1>*/}
        {/*    <p>Spotify</p>*/}
        {/*    <button>Play</button>*/}
        {/*    <div className={"icons"}>*/}
        {/*      <div className={"iconsHeart"}>*/}
        {/*      */}
        {/*      </div>*/}
        {/*      <div className={"iconsDots"}>*/}
        {/*      */}
        {/*      </div>*/}
        {/*    </div>*/}
        {/*    <p>Minimalism, electronica and modern classical to concentrate to.</p>*/}
        {/*  </div>*/}
        {/*  <div className={"playlistPageSongs"}>*/}
        {/*    <ul>*/}
        {/*      <li>Song 1</li>*/}
        {/*      <li>Song 2</li>*/}
        {/*      <li>Song 3</li>*/}
        {/*      <li>Song 4</li>*/}
        {/*      <li>Song 5</li>*/}
        {/*    </ul>*/}
        {/*  </div>*/}
        {/*</div>*/}
      </div>
  )
}

export default PlaylistPage