import React from 'react';
import {ReactComponent as WatchLaterIcon} from "../svgs/youtube/watchLater.svg";
import {ReactComponent as YTAddToQueueIcon} from "../svgs/youtube/addToQueue.svg";
import {ReactComponent as CheckMarkIcon} from "../svgs/tick.svg";
import {ReactComponent as CloseIcon} from "../svgs/mee6/closeButton.svg";
import {ReactComponent as ThreeDotsIcon} from "../svgs/dropdownDots.svg";
import {ReactComponent as DeviantartLogo} from "../svgs/deviantart/deviantartLogo.svg";
import {ReactComponent as DeviantartText} from "../svgs/deviantart/deviantartText.svg";
import {ReactComponent as CreditCard} from "../svgs/credit-card.svg";
import {ReactComponent as AdditionalActions} from "../svgs/discord/additionalActions.svg";
import {ReactComponent as StaffBadge} from "../svgs/discord/badges/staff.svg";
import {ReactComponent as PartnerBadge} from "../svgs/discord/badges/partnered.svg";
import {ReactComponent as ModerationBadge} from "../svgs/discord/badges/moderator.svg";
import {ReactComponent as HypesquadBadge} from "../svgs/discord/badges/hypesquad/hypesquad.svg";
import {ReactComponent as BrillanceBadge} from "../svgs/discord/badges/hypesquad/brilliance.svg";
import {ReactComponent as BalanceBadge} from "../svgs/discord/badges/hypesquad/balance.svg";
import {ReactComponent as BraveryBadge} from "../svgs/discord/badges/hypesquad/bravery.svg";
import {ReactComponent as BugHunterBadge} from "../svgs/discord/badges/bugHunter.svg";
import {ReactComponent as BugHunterGoldBadge} from "../svgs/discord/badges/bugHunterGold.svg";
import {ReactComponent as EarlySupporterBadge} from "../svgs/discord/badges/earlySupporter.svg";
import {ReactComponent as NitroBadge} from "../svgs/discord/badges/nitro.svg";
import {ReactComponent as ServerBoosterBadge} from "../svgs/discord/badges/boosts/sixMonths.svg";
import {ReactComponent as ExternalLink} from "../svgs/externalLink.svg";
import {ReactComponent as VerifiedBackground} from "../svgs/flowerBackground.svg";
import {ReactComponent as VerifiedCheck} from "../svgs/checkmark2.svg";
import {ReactComponent as PlayButton} from "../svgs/play.svg";
import {ReactComponent as VerifedBackgroundExtra} from "../svgs/verifiedUserExtra.svg";

import {ReactComponent as NotificationsIcon} from "../svgs/notifications.svg";
import {ReactComponent as SettingsIcon} from "../svgs/settings.svg";
import {ReactComponent as HelpIcon} from "../svgs/question.svg";

import userIcon from "../pngs/WolfDiscordImage.png";
import steamIcon from "../pngs/steam.png";
import githubIcon from "../pngs/github.png";
import battlenetIcon from "../pngs/battlenet.png";
import twitchIcon from "../pngs/twitch.png";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

const AssetsPage = () => {
  return (
      <div className={"assetsPage"}>
        <p>This page is for practice copying sections of websites from scratch. It will not be part of the final product.</p>

        <h2>Some buttons from youtube.</h2>

        <div className={"youtubeAssets"}>
          <div className={"buttonTesting"}>
            <WatchLaterIcon/>
          </div>

          <div className={"buttonTesting"}>
            <YTAddToQueueIcon/>
          </div>

          <div className={"premiereText"}>
            <span>Premiere</span>
          </div>
        </div>

        <h2>The premium notification from Mee6</h2>

        <div className={"premiumRequired"}>
          <div className={"closeMenuButton"}>
            <button>
              <CloseIcon/>
            </button>
          </div>
          <div className={"premiumContentWrapper"}>
            <div className={"premiumImage"}/>
            <div className={"premiumPerks"}>
              <div>
                <h3>You discovered a <b>Premium</b> feature.</h3>
              </div>
              <div>
                <h3>Upgrade to unlock it.</h3>
              </div>
              <ul>
                <li>
                  <CheckMarkIcon/>
                  <span>Fully <b>refundable</b> for 7 days.</span>
                </li>
                <li>
                  <CheckMarkIcon/>
                  <span><b>Transferable</b> to another server.</span>
                </li>
                <li>
                  <CheckMarkIcon/>
                  <span><b>Custom bot</b> option for free.</span>
                </li>
                <li>
                  <CheckMarkIcon/>
                  <span>Unlock all <b>social connectors.</b></span>
                </li>
              </ul>
              <div className={"callToAction"}>
                <button className={"premiumButton"}>
                  <div>
                    <span>Upgrade to Premium</span>
                    {/*  SVG */}
                  </div>
                </button>
                <button className={"notNowButton"}>
                  <span>Not Now</span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <h2>User Profile</h2>

        <div className={"userProfileWrapper"}>
          <div className={"userProfile"}>
            <span>
              <img src="https://a.deviantart.net/avatars-big/r/a/ramiras.png?8" alt="Profile"/>
            </span>
            <div className={"userInfo"}>
              <h1>Username</h1>
              <div className={"userSubInfo"}>
                <span>500 Watchers</span>
                <span>100.2K Page Views</span>
                <span>540 Comments</span>
                <span>79 Favorites</span>
              </div>
            </div>
          </div>
        </div>

        <h2>Deviantart login page</h2>

        <div className={"deviantartLoginWrapper"}>
          <div className={"deviantartLoginPage"}>
            <div className={"loginPageLeft"}>
              <div className="loginPageLeftContent">
                <div>
                  <a href="#1">
                    <div className={"logoArea"}>
                      <span className={"logoImage"}>
                        <DeviantartLogo/>
                      </span>
                      <span className={"logoText"}>
                        <DeviantartText/>
                      </span>
                    </div>
                  </a>
                  <h1>JOIN THE LARGEST ART COMMUNITY IN THE WORLD</h1>
                  <p>Explore and discover art, become a better artist, connect with others over mutual hobbies, or buy
                    and sell work – you can do it all here.</p>
                </div>
                <div>
                  <div>Art By</div>
                  <a href="#1">Lej-Ah</a>
                </div>
              </div>
            </div>
            <div className={"loginPageRight"}>
              <button className={"closeMenuButton"}>
                <CloseIcon/>
              </button>
              <div className="loginPageRightContent">
                <h2>Log In</h2>
                <p>
                  <span>Become a deviant.</span>
                  <a href="/join?referer=https%3A%2F%2Fwww.deviantart.com%2F">Join</a>
                </p>
                <div className={"loginPageRightContentForm"}>
                  <form>
                    <div className={"username"}>
                      <div>
                        <div>
                          <input type="text" name="username" placeholder={"Username"} autoFocus="" spellCheck="true"
                                 step="1"/>
                        </div>
                      </div>
                    </div>
                    <div className={"password"}>
                      <input type="password" name="password" placeholder={"Password"} spellCheck="true" step="1"/>
                    </div>
                    <label>
                      <div>
                        <CheckMarkIcon/>
                      </div>
                      <span>Keep me logged in</span>
                    </label>
                    <div>
                      <button>Log In</button>
                      <a href="#/users/forgot?kind=password">Forgot your username or password?</a>
                    </div>
                  </form>
                </div>
                <div className={"loginPageRightContentFooter"}>
                  By clicking Log In, I confirm that I have read and agree to the
                  DeviantArt <a href="https://www.deviantart.com/about/policy/service">Terms of Service</a>, <a
                    href="https://www.deviantart.com/about/policy/privacy">Privacy Policy</a>, and to receive
                  emails and updates.
                </div>
              </div>
            </div>
          </div>
        </div>

        <h2>Mr. Steal your information.</h2>

        <div className={"creditCards"}>
          <div className={"creditCardWrapper"}>
            <div className={"creditCardHeader"}>
              <span>Credit Card</span>
              <div>
                <button>
                  <ThreeDotsIcon/>
                </button>
              </div>
            </div>

            <div className={"creditCard"}>
              <div>
                {/*<img src={CreditCardImage} alt="Credit Card"/>*/}
                <CreditCard/>
              </div>
              <div className={"creditCardInner"}>
                <div>XXXX-XXXX-XXXX-6789</div>
                <span>$1,560</span>
              </div>
            </div>
          </div>

          <div className={"creditCardWrapper2"}>
            <div className={"creditCardHeader2"}>
              <span>My Credit Card</span>
              <div className={"customCreditCardWrapper"}>
                <div className={"cardProvider"}>
                  <span><i>VISA</i></span>
                </div>
                <div className={"cardSpacer"}/>
                <div className={"cardNumber"}>1234 5678 9012 3456</div>
                <div className={"cardInfo"}>
                  <span>12/21</span>
                  <span>Card Holder</span>
                </div>
              </div>
            </div>
            <div className={"creditCard2"}>
              <div className={"creditCardTitle"}>
                <span>Card Number:</span>
                <span>Card Status:</span>
              </div>
              <div className={"creditCardValues"}>
                <span>1234 5678 9012 3456</span>
                <span>Active</span>
              </div>
            </div>
          </div>
        </div>

        <h2>Discord User Profile Card</h2>

        <div className={"userProfileCardWrapper"}>
          <div className={"userProfileCard"}>
            <div className={"userProfileHeader"}>
              <div className={"headerBanner"}>
                <div className={"betaBadge"}>
                  Beta
                </div>
              </div>
              <div className={"profileHeader"}>
                <img src={userIcon} alt="avatar"/>
                <div className={"profileHeaderInner"}>
                  <div className={"profileHeaderLeft"}>
                    <StaffBadge/>
                    <PartnerBadge/>
                    <ModerationBadge/>
                    <HypesquadBadge/>
                    <BraveryBadge/>
                    <BrillanceBadge/>
                    <BalanceBadge/>
                    <BugHunterBadge/>
                    <BugHunterGoldBadge/>
                    <div className={"specialBadges"}>
                      <EarlySupporterBadge/>
                    </div>
                    <div className={"specialBadges"}>
                      <NitroBadge/>
                    </div>
                    <ServerBoosterBadge/>
                  </div>
                  <div className={"profileHeaderRight"}>
                    <div className={"profileHeaderRightInner"}>
                      <button className={"callToActionButton"}>
                        <div className={"buttonText"}>Send Friend Request</div>
                      </button>
                      <button>
                        <AdditionalActions/>
                      </button>
                    </div>
                  </div>
                </div>

                <div className={""}>
                  <div className={"username"}>
                    <span className={"name"}>Nothixal</span>
                    <span className={"discriminator"}>#4875</span>
                  </div>
                  <div className={"customStatus"}>
                    Developing better things. (Or something of that sort)
                  </div>
                </div>
              </div>
            </div>
            <div className={"userProfileBody"}>
              <Tabs>
                <TabList>
                  <Tab>User Info</Tab>
                  <Tab>Activity</Tab>
                  <Tab>Mutual Servers</Tab>
                  <Tab>Mutual Friends</Tab>
                </TabList>
                <TabPanel>
                  <div className={"userInfoSection"}>
                    <h3>About Me</h3>
                    <p>Mac and Cheese enthusiast and professional fun haver</p>

                    <h3>Note</h3>
                    <div>
                      <input type={"text"} placeholder={"Click to add a note"}/>
                    </div>

                    <div className={"userSocialLinksWrapper"}>
                      <div className={"userSocialLinks"}>
                        <div className="connectedAccount">
                          <img src={battlenetIcon} alt="BattleNet"/>
                          <div className={"connectedAccountInner"}>
                            <div className={"connectedAccountName"}>Account Name</div>
                            <div className={"verifiedWrapper"}>
                              <VerifiedBackground/>
                              <div className={"childContainer"}>
                                <VerifiedCheck/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="connectedAccount">
                          <img src={githubIcon} alt="Github"/>
                          <div className={"connectedAccountInner"}>
                            <div className={"connectedAccountName"}>Account Name</div>
                            <div className={"verifiedWrapper"}>
                              <VerifiedBackground/>
                              <div className={"childContainer"}>
                                <VerifiedCheck/>
                              </div>
                            </div>
                          </div>
                          <a href="#2">
                            <ExternalLink/>
                          </a>
                        </div>
                        <div className="connectedAccount">
                          <img src={steamIcon} alt="Steam"/>
                          <div className={"connectedAccountInner"}>
                            <div className={"connectedAccountName"}>Account Name</div>
                            <div className={"verifiedWrapper"}>
                              <VerifiedBackground/>
                              <div className={"childContainer"}>
                                <VerifiedCheck/>
                              </div>
                            </div>
                          </div>
                          <a href="#2">
                            <ExternalLink/>
                          </a>
                        </div>
                        <div className="connectedAccount">
                          <img src={twitchIcon} alt="Twitch"/>
                          <div className={"connectedAccountInner"}>
                            <div className={"connectedAccountName"}>Account Name</div>
                            <div className={"verifiedWrapper"}>
                              <VerifiedBackground/>
                              <div className={"childContainer"}>
                                <VerifiedCheck/>
                              </div>
                            </div>
                          </div>
                          <a href="#2">
                            <ExternalLink/>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </TabPanel>
                <TabPanel>
                  Activity
                </TabPanel>
                <TabPanel>
                  Mutual Servers
                </TabPanel>
                <TabPanel>
                  Mutual Friends
                </TabPanel>
              </Tabs>
            </div>
            {/*<div className={"userProfileFooter"}>*/}
            {/*  Message*/}
            {/*</div>*/}
          </div>
        </div>

        <h2>Multicolor Border</h2>

        <div className={"multicoloredBorders"}>
          <div className="multicolorBorder">
            <div className={"innerCircle"}/>
            <img src={userIcon} alt="Avatar"/>
          </div>

          <div className="color-wheel"/>
        </div>

        <h2>Button Spacing</h2>

        <div className={"svgSpacing"}>
          <div className="section">
            <button>
              <HelpIcon/>
            </button>
          </div>
          <div className="section">
            <button>
              <NotificationsIcon/>
            </button>
          </div>
          <div className="section">
            <button>
              <SettingsIcon/>
            </button>
          </div>
          <div className="section">
            <button>
              <img src={userIcon} alt="User"/>
            </button>
          </div>
        </div>

        <h2>Image Button Layout</h2>

        <div className={"imageButtonSpacing"}>
          <div className="playlistCardWrapper">
            <div className="playlistCardImage">
              <img src="https://newjams-images.scdn.co/image/ab676477000033ad/dt/v3/discover-weekly/TJRWJcNvS7TN4yQr_YGyXt8yjT3OXAQUjV1TUCmEVn5OVlcKj45wNhViawfh9IAic_xdH0o4GMHfD6oPLQRHSUxTcklbnsfwg2Su0SwH_qg=/MjA6NzE6NzBUMjAtMjEtMQ==" alt=""/>
            </div>
            <div className={"playlistCardContent"}>
              <div className="playlistCardText">
                <span>Discover Weekly</span>
              </div>
              <div className="playlistCardButton">
                <button>
                  <PlayButton/>
                </button>
              </div>
            </div>
          </div>
          <div className="playlistCardWrapper">
            <div className="playlistCardImage">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb56ef18c5c7d1e94b5bc36de0/1/en/default" alt=""/>
            </div>
            <div className="playlistCardContent">
              <div className="playlistCardText">
                <span>Daily Mix 1</span>
              </div>
              <div className="playlistCardButton">
                <button>
                  <PlayButton/>
                </button>
              </div>
            </div>
          </div>
          <div className="playlistCardWrapper">
            <div className="playlistCardImage">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb71c5d7893f5a7c186b239a78/2/en/default" alt=""/>
            </div>
            <div className="playlistCardContent">
              <div className="playlistCardText">
                <span>Daily Mix 2</span>
              </div>
              <div className="playlistCardButton">
                <button>
                  <PlayButton/>
                </button>
              </div>
            </div>
          </div>
          <div className="playlistCardWrapper">
            <div className="playlistCardImage">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb26440e0d39e887915c308778/3/en/default" alt=""/>
            </div>
            <div className="playlistCardContent">
              <div className="playlistCardText">
                <span>Daily Mix 3</span>
              </div>
              <div className="playlistCardButton">
                <button>
                  <PlayButton/>
                </button>
              </div>
            </div>
          </div>
          <div className="playlistCardWrapper">
            <div className="playlistCardImage">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5ebd1ca725c8d1062ebaa1ad069/4/en/default" alt=""/>
            </div>
            <div className="playlistCardContent">
              <div className="playlistCardText">
                <span>Daily Mix 4</span>
              </div>
              <div className="playlistCardButton">
                <button>
                  <PlayButton/>
                </button>
              </div>
            </div>
          </div>
        </div>

        <h2>Verified Users</h2>

        <div className="verifiedUsersWrapper">
          <div className={"verifiedUser"}>
            <img src={userIcon} alt="User"/>
            <span>Nothixal</span>
          </div>
          <div className={"verifiedUser2"}>
            <img src={userIcon} alt="User"/>
            <div className={"userBackground"}>
              <span>Super Long Username Designed To Break All Arbitrary Character Limits</span>
              <VerifedBackgroundExtra/>
            </div>

            {/*<div className="heart"/>*/}
          </div>
        </div>

        <div className={"paymentMethodIcons"}>

        </div>

      </div>
  );
};

export default AssetsPage;