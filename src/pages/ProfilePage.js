import React from 'react';
import userIcon from "../pngs/WolfDiscordImage.png";

const ProfilePage = () => {
  return (
      <div className={"profilePage"}>
        Profile Page
        <div className={"profilePageHeaderWrapper"}>
          <div className={"profilePageHeader"}>
            <div className={"userProfileWrapper"}>
              <div className={"userProfile"}>
                <span>
                  {/* "https://a.deviantart.net/avatars-big/r/a/ramiras.png?8" */}
                  <img src={userIcon} alt="Profile"/>
                </span>
                <div className={"userInfo"}>
                  <h1>Username</h1>
                  <div className={"userSubInfo"}>
                    <span>500 Watchers</span>
                    <span>100.2K Page Views</span>
                    <span>540 Comments</span>
                    <span>79 Favorites</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default ProfilePage;