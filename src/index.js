import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Rayna from './Rayna';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter as Router} from "react-router-dom";
import 'font-awesome/css/font-awesome.min.css';

// const help = (
//     <div>
//       <header>Head</header>
//       <body>Body</body>
//       <footer>Foot</footer>
//     </div>
// );
//
// const test = (
//     <header>
//       Header
//     </header>
// );
//
// const routing = (
//     <Router>
//       <h1>Testing123</h1>
//     </Router>
// );

ReactDOM.render(
    <React.StrictMode>
      {/*<header>Header</header>*/}
      <Router>
        {/*<body>Test</body>*/}
        <Rayna/>
      </Router>
      {/*<footer>Footer</footer>*/}
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
