import React, {useRef} from 'react';
import {ReactComponent as LogoIcon} from '../svgs/logo.svg';
import {ReactComponent as LogoutIcon} from '../svgs/logout.svg';
import {ReactComponent as BillingIcon} from '../svgs/billing.svg';
import {ReactComponent as PlaceholderIcon} from '../svgs/placeholder.svg';
import {ReactComponent as MyServersIcon} from '../svgs/MyServers.svg';
import {ReactComponent as FeedbackIcon} from "../svgs/feedback.svg";
import {ReactComponent as NotificationsIcon} from "../svgs/notifications.svg";
import {ReactComponent as SettingsIcon} from "../svgs/settings.svg";
import {ReactComponent as ServerSVG} from "../svgs/server.svg";
import userIcon from "../pngs/WolfDiscordImage.png";
import {useDetectOutsideClick} from "../useDetectOutsideClick";
import {Link, NavLink} from "react-router-dom";

const Header = (props) => {
  const dropdownRef = useRef(null);
  const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  const onClick2 = () => setIsActive(!isActive);

  // console.log(props.collapsed)

  return (
      /*
      * LAYOUT
      *
      * Header
      *   Search Bar
      *   Toolbar
      *     Go Premium Button
      *     Notifications
      *     Settings
      *     Account Button
      *
      * */

      <header className={"upperNav"}>

        <div className={`sidebarSection ${props.collapsed ? "collapsed" : "expanded"}`}>
          <div className="logo">
            <Link to={"/"}>
              <LogoIcon/>
              <span>RAYNA</span>
            </Link>
          </div>
        </div>

        <div className={"separator"}/>

        <div className={"mainUpperNav"}>
          <div className="searchContainer">
            <div className={"searchArea"}>
              <div className={"searchBarFilterButton"}>
                <button>Filter</button>
              </div>

              <div className={"separator"}/>

              <form>
                <div className={"searchBar"}>
                  <i className={"fa fa-search"}/>
                  <input type="text" placeholder="Search" name="search"/>
                </div>
              </form>

              <div className={"separator"}/>

              <div className={"searchBarButton"}>
                <button>X</button>
              </div>

              <div className={"separator"}/>

              <div className={"searchBarButton"}>
                <button>
                  <i className={"fa fa-search"}/>
                </button>
              </div>
            </div>
            {/*<form>*/}
            {/*  <div className={"searchBarFilterButton"}>*/}
            {/*    <button>Filter</button>*/}
            {/*  </div>*/}

            {/*  <div className={"separator"}/>*/}

            {/*  <div className={"searchBar"}>*/}
            {/*    <i className={"fa fa-search"}/>*/}
            {/*    <input type="text" placeholder="Search" name="search"/>*/}
            {/*  </div>*/}

            {/*  <div className={"separator"}/>*/}

            {/*  <div className={"searchBarButton"}>*/}
            {/*    <button>X</button>*/}
            {/*  </div>*/}

            {/*  <div className={"separator"}/>*/}

            {/*  <div className={"searchBarButton"}>*/}
            {/*    <button>*/}
            {/*      <i className={"fa fa-search"}/>*/}
            {/*    </button>*/}
            {/*  </div>*/}
            {/*  /!*<button type="submit"><i className="fa fa-search"/></button>*!/*/}
            {/*</form>*/}
          </div>

          <div className={"premium"}>
            <Link to={"/premium"}>
              <button className={"premiumButton"}>
                Go Premium
              </button>
            </Link>
          </div>

          <div className={"separator"}/>

          <div className={"notifications"}>
            <NotificationsIcon/>
          </div>

          <div className={"settings"}>
            <SettingsIcon/>
          </div>

          <div className={"settings"}>
            <NavLink to={"/servers"}>
              <ServerSVG/>
            </NavLink>
          </div>

          <div onClick={onClick2} className={"account"}>
            <button className={"accountButton"}>
              <img src={userIcon} alt={"User Account"}/>
              {/*<span className={"accountButtonText"}>ThisIsATemporaryTestForDiscord32#5431</span>*/}
              {/*<div className={"accountDropdownCaretWrapper"}>*/}
              {/*  <DropdownIcon className={`accountDropdownCaret ${isActive ? "open" : "closed"}`}/>*/}
              {/*</div>*/}
            </button>

            <ul ref={dropdownRef} className={`accountDropdownMenu ${isActive ? "active" : "inactive"}`}>
              <header className={"accountDropdownHeader"}>
                <img src={userIcon} alt={"Account Icon"}/>
                <span>Nothixal#4875</span>
              </header>
              <li>
                <MyServersIcon/>
                <span>My Servers</span>
              </li>
              <li>
                <PlaceholderIcon/>
                <span>Edit Rank Card</span>
              </li>
              <li>
                <BillingIcon/>
                <span>Billing</span>
              </li>
              <li className={"logout"}>
                <LogoutIcon/>
                <span>Log Out</span>
              </li>
              <li>
                <FeedbackIcon/>
                <span>Send Feedback</span>
              </li>
            </ul>
          </div>
        </div>
      </header>
  )
}

export default Header