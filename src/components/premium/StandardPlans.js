import React from 'react';

const StandardPlans = () => {
  return (
      <div className={"plans"}>
        <div className={"cardWrapper"}>
          <div className={"cardHeader monthly"}>
            <span>Monthly</span>
            <div className={"cardPrice"}>
              <span>$4.99<small>/month</small></span>
            </div>
            {/*<button>Go Premium</button>*/}
          </div>
          {/*<div className={"cardBody"}>*/}
          {/*  <span className={"bonus"}>Better Audio Quality</span>*/}
          {/*  <span className={"bonus"}>Longer Songs</span>*/}
          {/*  <span className={"bonus"}>Volume Control</span>*/}
          {/*  <span className={"bonus"}>Audio Effects</span>*/}
          {/*  <span className={"bonus"}>Playlists</span>*/}
          {/*  <span className={"bonus"}>Filters</span>*/}
          {/*  /!*<span className={"bonus"}>Always Playing</span>*!/*/}
          {/*  /!*<span className={"bonus"}>Extra Bots</span>*!/*/}
          {/*</div>*/}
        </div>

        <div className={"cardWrapper"}>
          <div className={"cardHeader annually"}>
            <span>Quarterly</span>
            <div className={"cardPrice"}>
              <span>$19.99<small>/year</small></span>
            </div>
            {/*<button>Go Premium</button>*/}
          </div>
          {/*<div className={"cardBody"}>*/}
          {/*  <span className={"bonus"}>Better Audio Quality</span>*/}
          {/*  <span className={"bonus"}>Longer Songs</span>*/}
          {/*  <span className={"bonus"}>Volume Control</span>*/}
          {/*  <span className={"bonus"}>Audio Effects</span>*/}
          {/*  <span className={"bonus"}>Playlists</span>*/}
          {/*  <span className={"bonus"}>Filters</span>*/}
          {/*  /!*<span className={"bonus"}>Always Playing</span>*!/*/}
          {/*  /!*<span className={"bonus"}>Extra Bots</span>*!/*/}
          {/*</div>*/}
        </div>

        <div className={"cardWrapper"}>
          <div className={"cardHeader lifetime"}>
            <span>Annually</span>
            <div className={"cardPrice"}>
              <span>$59.99<small>/year</small></span>
            </div>
            {/*<button>Go Premium</button>*/}
          </div>
          {/*<div className={"cardBody"}>*/}
          {/*  <span className={"bonus"}>Better Audio Quality</span>*/}
          {/*  <span className={"bonus"}>Longer Songs</span>*/}
          {/*  <span className={"bonus"}>Volume Control</span>*/}
          {/*  <span className={"bonus"}>Audio Effects</span>*/}
          {/*  <span className={"bonus"}>Playlists</span>*/}
          {/*  <span className={"bonus"}>Filters</span>*/}
          {/*  /!*<span className={"bonus"}>Always Playing</span>*!/*/}
          {/*  /!*<span className={"bonus"}>Extra Bots</span>*!/*/}
          {/*</div>*/}
        </div>

        <div className={"cardWrapper"}>
          <div className={"cardHeader custom"}>
            <span>Lifetime</span>
            <div className={"cardPrice"}>
              <span>$299.99</span>
            </div>
          </div>
          {/*<div className={"cardBody"}>*/}
          {/*  <span className={"bonus"}>Better Audio Quality</span>*/}
          {/*  <span className={"bonus"}>Longer Songs</span>*/}
          {/*  <span className={"bonus"}>Volume Control</span>*/}
          {/*  <span className={"bonus"}>Audio Effects</span>*/}
          {/*  <span className={"bonus"}>Playlists</span>*/}
          {/*  <span className={"bonus"}>Filters</span>*/}
          {/*  /!*<span className={"bonus"}>Always Playing</span>*!/*/}
          {/*  /!*<span className={"bonus"}>Extra Bots</span>*!/*/}
          {/*</div>*/}
        </div>
      </div>
  );
};

export default StandardPlans;