import React, {useState} from 'react';

const CustomPlans = () => {
  const [checkboxVal, setCheckboxVal] = useState(false);

  const setCheckboxValue = () => {
    setCheckboxVal(checkboxVal);
  }

  const [price, setPrice] = useState(0.99);
  const [numberOfServers, setNumberOfServers] = useState(1);

  const increasePrice = () => {
    if (numberOfServers >= 5) {
      setPrice(price + 0.50);
    } else {
      setPrice(price + 1.00);
    }
  }

  const decreasePrice = () => {
    if (price <= 0.99) {
      return;
    }

    if (numberOfServers > 5) {
      setPrice(price - 0.50);
    } else {
      setPrice(price - 1.00);
    }

  }

  const incrementNumber = () => {
    if (numberOfServers >= 100) {
      return;
    }

    increasePrice();
    setNumberOfServers(numberOfServers + 1);
  }

  const decrementNumber = () => {
    if (numberOfServers <= 1) {
      return;
    }

    decreasePrice();
    setNumberOfServers(numberOfServers - 1);
  }

  return (
      <div className={"customPlans"}>
        <div className={"cardWrapperCustom"}>
          <div className={"customPlanTaglineWrapper"}>
            <div className={"customPlanTagline right"}>
              <span>Don't need all those extra features?</span>
              <span>Looking for a cheaper plan?</span>
              <span>Rayna's got you covered.</span>
            </div>
          </div>

          <div className={"customPlanDefaults"}>
            <h3>These features are included by default with any premium purchase!</h3>
            <div className={"premiumDefaults"}>
              <span>Better Audio Quality</span>
              <span>Volume Control</span>
              <span>Longer Songs</span>
              <span>Dashboard Badge</span>
              <span>Priority Support</span>
            </div>
          </div>
        </div>
        <div className={"extraFeatures"}>
          {/*<span>Extra Features Section</span>*/}
          <ul>
            <li>
              <div className="additionalFeatures">
                <div className={"extraFeature"}>
                  <div className={"extraFeatureTitle"}>Always Playing</div>
                  <div className={"extraFeatureDescription"}>The bot stays in the voice channel 24/7, ready to play when you join.
                  </div>
                </div>
                <div className={"featurePricing"}>
                  <div><span className={"featurePrice"}>$1.99</span>/mo</div>
                  <div>
                    <form>
                      <input
                          name={"added"}
                          type={"checkbox"}
                          onChange={() => setCheckboxValue(!checkboxVal)}
                      />
                      <label>Add</label>
                    </form>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="additionalFeatures">
                <div className={"extraFeature"}>
                  <div className={"extraFeatureTitle"}>Audio Effects</div>
                  <div className={"extraFeatureDescription"}>Allows you to speed up, slow down, nightcore and bass boost songs.
                  </div>
                </div>
                <div className={"featurePricing"}>
                  <div><span className={"featurePrice"}>$0.49</span>/mo</div>
                  <div>
                    <form>
                      <input
                          name={"added"}
                          type={"checkbox"}
                          onChange={() => setCheckboxValue(!checkboxVal)}
                      />
                      <label>Add</label>
                    </form>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="additionalFeatures">
                <div className={"extraFeature"}>
                  <div className={"extraFeatureTitle"}>Autoplay</div>
                  <div className={"extraFeatureDescription"}>When your queue ends, Rayna will automatically play something else.
                  </div>
                </div>
                <div className={"featurePricing"}>
                  <div><span className={"featurePrice"}>$0.49</span>/mo</div>
                  <div>
                    <form>
                      <input
                          name={"added"}
                          type={"checkbox"}
                          onChange={() => setCheckboxValue(!checkboxVal)}
                      />
                      <label>Add</label>
                    </form>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="additionalFeatures">
                <div className={"extraFeature"}>
                  <div className={"extraFeatureTitle"}>Extra Bots</div>
                  <div className={"extraFeatureDescription"}>To support multiple voice channels, get multiple Premium Rayna's.
                  </div>
                </div>
                <div className={"featurePricing"}>
                  <div><span className={"featurePrice"}>$2.99</span>/mo</div>
                  <div>
                    <form>
                      <input
                          name={"added"}
                          type={"checkbox"}
                          onChange={() => setCheckboxValue(!checkboxVal)}
                      />
                      <label>Add</label>
                    </form>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="additionalFeatures">
                <div className={"extraFeature"}>
                  <div className={"extraFeatureTitle"}>Total Servers</div>
                  <div className={"extraFeatureDescription"}>How many servers do you need premium for?</div>
                </div>
                <div className={"featurePricing"}>
                  <div><span className={"featurePrice"}>${price}</span>/mo</div>
                  <button onClick={decrementNumber}>-</button>
                  <div>
                    {numberOfServers}
                  </div>
                  <button onClick={incrementNumber}>+</button>
                </div>
              </div>
            </li>
          </ul>

          <div className={"paymentButton"}>
            <button>Continue to Payment</button>
          </div>
        </div>
      </div>
  );
};

export default CustomPlans;