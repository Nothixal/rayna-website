import React from 'react';
import {FaBars} from "react-icons/fa";

const TestComponent = (props) => {
  return (
      <div>
        <div onClick={props.handleCollapsedChange}>
          <FaBars />
          TEST
        </div>
      </div>
  );
};

export default TestComponent;