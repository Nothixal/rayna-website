import React from 'react';
import userIcon from "../../../pngs/WolfDiscordImage.png";
import {ReactComponent as MyServersIcon} from "../../../svgs/MyServers.svg";
import {ReactComponent as PlaceholderIcon} from "../../../svgs/placeholder.svg";
import {ReactComponent as BillingIcon} from "../../../svgs/billing.svg";
import {ReactComponent as LogoutIcon} from "../../../svgs/logout.svg";
import {ReactComponent as FeedbackIcon} from "../../../svgs/feedback.svg";
import {Link} from "react-router-dom";

const AccountDropdown = () => {
  return (
      <div className={"accountDropdown"}>
        <header className={"dropdownHeader"}>
          <img src={userIcon} alt="User"/>
          <span>Nothixal#4875</span>
        </header>
        <ul>
          <li>
            <Link to={"/servers"}>
              <MyServersIcon/>
              <span>My Servers</span>
            </Link>
          </li>
          <li>
            <Link to={"/rankcard"}>
              <PlaceholderIcon/>
              <span>Edit Rank Card</span>
            </Link>
          </li>
          <li>
            <Link to={"/billing"}>
              <BillingIcon/>
              <span>Billing</span>
            </Link>
          </li>
          <li className={"logout"}>
            <Link>
              <LogoutIcon/>
              <span>Log Out</span>
            </Link>
          </li>
          <li>
            <Link>
              <FeedbackIcon/>
              <span>Send Feedback</span>
            </Link>
          </li>

        </ul>
      </div>
  );
};

export default AccountDropdown;