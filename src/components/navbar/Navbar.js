import React, {useRef} from 'react';
import {useDetectOutsideClick} from "../../useDetectOutsideClick";
import {Link} from "react-router-dom";
import {ReactComponent as NotificationsIcon} from "../../svgs/notifications.svg";
import {ReactComponent as SettingsIcon} from "../../svgs/settings.svg";
import userIcon from "../../pngs/WolfDiscordImage.png";
import {ReactComponent as AnnouncementIcon} from "../../svgs/announcement.svg";
import SidebarSection from "./SidebarSection";
import AccountDropdown from "./account/AccountDropdown";

const Navbar = (props) => {
  const dropdownRef = useRef(null);
  const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  const onClick = () => setIsActive(!isActive);

  const dropdownRef2 = useRef(null);
  const [isActive2, setIsActive2] = useDetectOutsideClick(dropdownRef2, false);
  const onClick2 = () => setIsActive2(!isActive2);

  // console.log(props.collapsed)

  return (
      /*
      * LAYOUT
      *
      * Navbar
      *   Search Bar
      *   Toolbar
      *     Go Premium Button
      *     Notifications
      *     Settings
      *     Account Button
      *
      * */

      <header className={"navbar"}>
        <SidebarSection {...props}/>

        <div className={"separator"}/>

        {/*<div className={"mainNavContent"}>*/}
          <div className={"navContentLeft"}>
            <div className={"navContentLeftInner"}>

            </div>
          </div>
          {/*<div className="searchContainer">*/}
          {/*  <div className={"searchArea"}>*/}
          {/*    <div className={"searchBarFilterButton"}>*/}
          {/*      <button>Filter</button>*/}
          {/*    </div>*/}

          {/*    <div className={"separator"}/>*/}

          {/*    <form>*/}
          {/*      <div className={"searchBar"}>*/}
          {/*        <i className={"fa fa-search"}/>*/}
          {/*        <input type="text" placeholder="Search" name="search"/>*/}
          {/*      </div>*/}
          {/*    </form>*/}

          {/*    <div className={"separator"}/>*/}

          {/*    <div className={"searchBarButton"}>*/}
          {/*      <button>X</button>*/}
          {/*    </div>*/}

          {/*    <div className={"separator"}/>*/}

          {/*    <div className={"searchBarButton"}>*/}
          {/*      <button>*/}
          {/*        <i className={"fa fa-search"}/>*/}
          {/*      </button>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*  /!*<form>*!/*/}
          {/*  /!*  <div className={"searchBarFilterButton"}>*!/*/}
          {/*  /!*    <button>Filter</button>*!/*/}
          {/*  /!*  </div>*!/*/}

          {/*  /!*  <div className={"separator"}/>*!/*/}

          {/*  /!*  <div className={"searchBar"}>*!/*/}
          {/*  /!*    <i className={"fa fa-search"}/>*!/*/}
          {/*  /!*    <input type="text" placeholder="Search" name="search"/>*!/*/}
          {/*  /!*  </div>*!/*/}

          {/*  /!*  <div className={"separator"}/>*!/*/}

          {/*  /!*  <div className={"searchBarButton"}>*!/*/}
          {/*  /!*    <button>X</button>*!/*/}
          {/*  /!*  </div>*!/*/}

          {/*  /!*  <div className={"separator"}/>*!/*/}

          {/*  /!*  <div className={"searchBarButton"}>*!/*/}
          {/*  /!*    <button>*!/*/}
          {/*  /!*      <i className={"fa fa-search"}/>*!/*/}
          {/*  /!*    </button>*!/*/}
          {/*  /!*  </div>*!/*/}
          {/*  /!*  /!*<button type="submit"><i className="fa fa-search"/></button>*!/*!/*/}
          {/*  /!*</form>*!/*/}
          {/*</div>*/}

          <div className={"navContentRight"}>
            <div className={"premium"}>
              <Link to={"/premium"}>
                <span>Go Premium</span>
              </Link>
            </div>

            {/*<div className={"separator"}/>*/}

            <div className="userControls">
              {/*<div className={"buttonContainer"}>*/}
              {/*  <button>*/}
              {/*    <HelpIcon/>*/}
              {/*  </button>*/}
              {/*</div>*/}

              <div onClick={onClick2} className={"buttonContainer"}>
                <button className={"notificationsButton"}>
                  <NotificationsIcon/>
                </button>
                <div ref={dropdownRef} className={`notificationsDropdownMenu ${isActive2 ? "active" : "inactive"}`}>
                  <header className={"notificationsDropdownHeader"}>
                    <Link to={"/notifications"}>
                      Your Notifications
                    </Link>
                    <Link to={"/settings/notifications"}>
                      <button>
                        <SettingsIcon/>
                      </button>
                    </Link>
                  </header>
                  <ul>
                    <li>
                      <Link to={"/servers"}>
                        <AnnouncementIcon/>
                        <div className={"notificationCard"}>
                          <div className={"notificationCardHeader"}>
                            <p>Usability improvements for collaborative Deploy Previews</p>
                            <time>3 days ago</time>
                          </div>
                          <p>Content</p>
                          <span>Call To Action</span>
                        </div>
                      </Link>
                    </li>
                    <li>
                      <Link to={"/servers"}>
                        <AnnouncementIcon/>
                        <div className={"notificationCard"}>
                          <div className={"notificationCardHeader"}>
                            <p>Want to help us improve our navigation? Take our online survey!</p>
                            <time>15 days ago</time>
                          </div>
                          <p>Content</p>
                          <span>Call To Action</span>
                        </div>
                      </Link>
                    </li>
                    <li>
                      <Link to={"/servers"}>
                        <AnnouncementIcon/>
                        <div className={"notificationCard"}>
                          <div className={"notificationCardHeader"}>
                            <p>On-demand Builders early access: faster builds, more flexible rendering</p>
                            <time>23 days ago</time>
                          </div>
                          <p>Content</p>
                          <span>Call To Action</span>
                        </div>
                      </Link>
                    </li>
                    <li className={"logout"}>
                      <Link to={"/servers"}>
                        <AnnouncementIcon/>
                        <div className={"notificationCard"}>
                          <div className={"notificationCardHeader"}>
                            <p>Better, faster Functions deployment: try the beta</p>
                            <time>a month ago</time>
                          </div>
                          <p>Content</p>
                          <span>Call To Action</span>
                        </div>
                      </Link>
                    </li>
                    <li>
                      <Link to={"/servers"}>
                        <AnnouncementIcon/>
                        <div className={"notificationCard"}>
                          <div className={"notificationCardHeader"}>
                            <p>Test drive new beta features with Netlify Labs</p>
                            <time>2 months ago</time>
                          </div>
                          <p>Content</p>
                          <span>Call To Action</span>
                        </div>
                      </Link>
                    </li>
                  </ul>
                  <footer className={"notificationsDropdownFooter"}>
                    <Link to={"/notifications"}>
                      Show All
                    </Link>
                  </footer>
                </div>
              </div>

              <div className={"buttonContainer"}>
                <Link to={"/settings"}>
                  <button>
                    <SettingsIcon/>
                  </button>
                </Link>
              </div>

              {/*<div className={"settings"}>*/}
              {/*  <NavLink to={"/servers"}>*/}
              {/*    <ServerSVG/>*/}
              {/*  </NavLink>*/}
              {/*</div>*/}
            </div>

            <div onClick={onClick} className={"account"}>
              <button className={"accountButton"}>
                <img src={userIcon} alt={"User Account"}/>
                {/*<span className={"accountButtonText"}>ThisIsATemporaryTestForDiscord32#5431</span>*/}
                {/*<div className={"accountDropdownCaretWrapper"}>*/}
                {/*  <DropdownIcon className={`accountDropdownCaret ${isActive ? "open" : "closed"}`}/>*/}
                {/*</div>*/}
              </button>

              {isActive ? <AccountDropdown/> : null}

              {/*<AccountDropdown {...props}/>*/}

              {/*<ul ref={dropdownRef} className={`accountDropdownMenu ${isActive ? "active" : "inactive"}`}>*/}
              {/*</ul>*/}
            </div>
          </div>
        {/*</div>*/}
      </header>
  )
};

export default Navbar;