import React from 'react';
import {ReactComponent as LogoIcon} from "../../svgs/logo.svg";
import {Link} from "react-router-dom";

const SidebarSection = (props) => {
  return (
      <div className={`sidebarSection ${props.collapsed ? "collapsed" : "expanded"}`}>
        {/*<div className="logo">*/}
        {/*  <Link to={"/"}>*/}
        {/*    <LogoIcon/>*/}
        {/*    <div className={"slide-right"}>*/}
        {/*      <span>RAYNA</span>*/}
        {/*    </div>*/}
        {/*  </Link>*/}
        {/*</div>*/}
        <div className="logoContainer">
          <Link to={"/"}>
            <div className={"logo"}>
              <LogoIcon/>
            </div>
            <div className={"logoText"}>
              <span>RAYNA</span>
            </div>
          </Link>
        </div>
        {/*<div className="separator"/>*/}
      </div>
  );
};

export default SidebarSection;