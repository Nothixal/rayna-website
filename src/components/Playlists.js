import React from 'react'
import {ReactComponent as PlayIcon} from "../svgs/play.svg";
import {Link} from 'react-router-dom'

const Playlists = (props) => {

  const playlists = [
    {
      id: 101,
      category_id: 3,
      name: "Home Playlist 1",
      img: "https://images.unsplash.com/photo-1615549332616-3922167e33f8?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 102,
      category_id: 3,
      name: "Home Playlist 2",
      img: "https://images.unsplash.com/photo-1615125468484-088e3dfcabb6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 103,
      category_id: 3,
      name: "Home Playlist 3",
      img: "https://images.unsplash.com/photo-1615801363715-4b886c3d91d7?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 104,
      category_id: 1,
      name: "Focus Playlist 1",
      img: "https://images.unsplash.com/photo-1615763282652-54418ea841fc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 105,
      category_id: 4,
      name: "Sunday Playlist 1",
      img: "https://images.unsplash.com/photo-1615835623871-11590e1eab95?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=637&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 106,
      category_id: 2,
      name: "Mood Playlist 1",
      img: "https://images.unsplash.com/photo-1615749254536-746d800ded1c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
      desc: "Lorem ipsum"
    },
    {
      id: 107,
      category_id: 2,
      name: "Mood Playlist 2",
      img: "https://images.unsplash.com/photo-1615749281200-59f7f151eab6?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
      desc: "Lorem ipsum"
    },
  ]

  const matchedPlaylists = playlists.filter(playlist => playlist.category_id === props.categoryId)

  return (
      <div className={"cardsWrapInner"}>
        {matchedPlaylists.map((playlist, id) => (
            <Link to={`/playlist/` + playlist.id} key={id}>
              <div className="card">
                <div className={"cardImage"}>
                  <img src={playlist.img} alt="Artwork"/>
                  <span className={"playIcon"}>
                    <PlayIcon/>
                  </span>
                </div>
                <div className={"cardContent"}>
                  <h3>{playlist.name}</h3>
                  <span>{playlist.desc}</span>
                </div>
              </div>
            </Link>
        ))}
      </div>
  )
}

export default Playlists