import React, {useState} from 'react';
import {ReactComponent as DropdownIcon} from "../svgs/dropdownAlt.svg";

const Accordion = ({children, title, isExpand = false}) => {
  const [expand, setExpand] = useState(isExpand);
  return (
      <div className="accordion">
        <div className={"accordionContentWrapper"}>
          <div className="title-box" onClick={() => setExpand(expand => !expand)}>
            <span className="title">{title}</span>
            <span className="icon">
              <DropdownIcon className={`chevron ${!expand ? ' down' : ''}`}/>
            </span>
            <div className="clearfix"/>
          </div>
          {expand && <div className="content">{children}</div>}
        </div>

      </div>
  )
}

export default Accordion;