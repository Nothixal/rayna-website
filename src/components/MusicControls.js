import React, {useEffect, useState} from 'react';

import {ReactComponent as LikeIcon} from '../svgs/like.svg';
import {ReactComponent as StopIcon} from '../svgs/trash.svg';
import {ReactComponent as PictureInPictureIcon} from '../svgs/pictureInPicture.svg';
import {ReactComponent as QueueIcon} from '../svgs/viewQueue.svg';
import {ReactComponent as DeviceSwitcherIcon} from '../svgs/deviceSwitcher.svg';
import {ReactComponent as VolumeIcon} from '../svgs/volume.svg';
import {ReactComponent as FullscreenIcon} from '../svgs/fullscreen.svg';

import {ReactComponent as ShuffleIcon} from '../svgs/shuffle.svg';
import {ReactComponent as PreviousIcon} from '../svgs/previous.svg';
import {ReactComponent as PlayIcon} from '../svgs/play.svg';
import {ReactComponent as PauseIcon} from '../svgs/spotify/pause.svg';
import {ReactComponent as NextIcon} from '../svgs/next.svg';
import {ReactComponent as Repeaticon} from '../svgs/repeat.svg';

const MusicControls = () => {
  // const [playbackTime, setPlaybackTime] = useState(0);
  const [volume, setVolume] = useState(100);

  const handleInput = event => {
    setVolume(event.target.value);
  };

  useEffect(() => {
    // Accessing scss variable "--background-color"
    // and "--text-color" using plain JavaScript
    // and changing the same according to the state of "darkTheme"
    const root = document.documentElement;
    root?.style.setProperty("--val", volume);
  }, [volume]);

  const [isPlaying, setIsPlaying] = useState(false);
  const handleTest = event => {
    setIsPlaying(!isPlaying);
  }

  return (
      <footer className="musicControls">
        <div className={"musicControlsContainer"}>

          <div className={"musicItemContainer fill30"}>
            <div className={"nowPlayingWidget"}>
              <img src="https://i.ytimg.com/vi/mNgeWabM-us/mqdefault.jpg" alt=""/>
              <div className={"trackInformation"}>
                <p>
                  <b>Top Text</b>
                </p>
                <p>Bottom Text</p>
              </div>
              <div className={"trackActions"}>
                <div className={"icon"}>
                  <LikeIcon/>
                </div>
                <div className={"icon"}>
                  <StopIcon/>
                </div>
                <div className={"icon"}>
                  <PictureInPictureIcon/>
                </div>
              </div>
            </div>
          </div>

          <div className={"musicItemContainer fill40"}>
            <div className={"playerControls"}>
              <div className={"playerControlsButtons"}>
                <div className={"playerControlsLeft"}>
                  <div className={"icon"}>
                    <ShuffleIcon/>
                  </div>
                  <div className={"icon"}>
                    <PreviousIcon/>
                  </div>
                </div>

                <button onClick={handleTest} className={"playPauseButton"}>
                  {isPlaying ? <PlayIcon/> : <PauseIcon/>}
                </button>

                <div className={"playerControlsRight"}>
                  <div className={"icon"}>
                    <NextIcon/>
                  </div>
                  <div className={"icon"}>
                    <Repeaticon/>
                  </div>
                </div>
              </div>
              <div className={"playerControlsPlaybackBar"}>
                <span>0:02</span>
                {/*<Slider/>*/}
                <input type="range" min="0" max="100" step="1"/>
                <span>3:47</span>
              </div>
            </div>
          </div>

          <div className={"musicItemContainer fill30"}>
            <div className={"extraControls"}>
              <div className={"icon"}>
                <QueueIcon/>
              </div>
              <div className={"icon"}>
                <DeviceSwitcherIcon/>
              </div>
              <div className={"icon"}>
                <VolumeIcon/>
              </div>
              <input onChange={handleInput} type="range" min="0" max="100" step="1" value={volume}/>
              <div className={"icon"}>
                <FullscreenIcon/>
              </div>
            </div>
          </div>
        </div>
      </footer>
  );
};

export default MusicControls;