import React from 'react';
import {NavLink} from 'react-router-dom';
import {ReactComponent as HomeIcon} from '../../svgs/home.svg';
import {ReactComponent as SearchIcon} from '../../svgs/search.svg';
import {ReactComponent as QueueIcon} from '../../svgs/queue.svg';
import {ReactComponent as PlaylistsIcon} from '../../svgs/playlists.svg';
import {ReactComponent as DashboardIcon} from '../../svgs/dashboard.svg';
import {ReactComponent as AudioLibraryIcon} from '../../svgs/audioLibrary.svg';
import {ReactComponent as AnalyticsIcon} from '../../svgs/analytics.svg';
import {ReactComponent as PaymentIcon} from '../../svgs/payment.svg';
import {ReactComponent as GiftIcon} from '../../svgs/gift.svg';
import {ReactComponent as PlaylistAltIcon} from '../../svgs/playlistsAlt.svg';
import {ReactComponent as CollapseIcon} from '../../svgs/collapse.svg';
import {ReactComponent as ServerSVG} from "../../svgs/server.svg";
import {ReactComponent as SettingsIcon} from "../../svgs/settings.svg";
import {ReactComponent as PremiumIcon} from "../../svgs/premiumBadge.svg";
import {ReactComponent as BillingIcon} from "../../svgs/billing.svg";
import {ReactComponent as NotificationsIcon} from "../../svgs/notifications.svg";
import GuildLogo from '../../pngs/guildIcon.png';
// import SidebarBackground from '../../sidebar-backgrounds/bg2.jpg';
import {Menu, MenuItem, ProSidebar, SidebarContent, SidebarFooter, SidebarHeader} from "react-pro-sidebar";
// import {useDetectOutsideClick} from "../useDetectOutsideClick";

const Sidebar = ({collapsed, toggled, handleToggleSidebar, handleCollapsedChange}) => {

  // const dropdownRef = useRef(null);
  // const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  // const onClick = () => setIsActive(!isActive);

  return (
      <ProSidebar
          collapsed={collapsed}
          toggled={toggled}
          breakPoint="md"
          // image={SidebarBackground}
          onToggle={handleToggleSidebar}>
        <SidebarHeader>
          <div className={"guildInfo"}>
            <img src={GuildLogo} alt={"Guild"}/>
            <div className={"guildInfoText" }>
              <h3 className={"guildName"}>General Testing Server With A Really Long Name</h3>
              {/*<h5 className={"memberCount"}>1,000,000 Members</h5>*/}
            </div>
          </div>
          {/*<div onClick={onClick} className={"guildInfo"}>*/}
          {/*  <button className={"serverSelectorButton"}>*/}
          {/*    <img src={userIcon} alt={"Server Selector"}/>*/}
          {/*    <span className={"serverSelectorText"}>General Testing Server</span>*/}
          {/*    <div className={"serverSelectorCaretWrapper"}>*/}
          {/*      <DropdownIcon className={`serverSelectorCaret ${isActive ? "open" : "closed"}`}/>*/}
          {/*    </div>*/}
          {/*  </button>*/}
          {/*</div>*/}
        </SidebarHeader>
        <SidebarContent>
          <Menu>
            {/*Add active={true} to the MenuItem to act as the active element.*/}
            <MenuItem icon={<HomeIcon />}>Home <NavLink to={"/"}/></MenuItem>
            <MenuItem icon={<DashboardIcon />}>Dashboard <NavLink to={"/dashboard"}/></MenuItem>
            <MenuItem icon={<PremiumIcon />}>Premium <NavLink to={"/premium"}/></MenuItem>
            <MenuItem icon={<BillingIcon />}>Billing <NavLink to={"/billing"}/></MenuItem>
            <MenuItem icon={<SearchIcon />}>Search <NavLink to="/search"/></MenuItem>
            <MenuItem icon={<QueueIcon />}>Queue <NavLink to="/queue"/></MenuItem>
            <MenuItem icon={<AudioLibraryIcon />}>Commands <NavLink to="/commands"/></MenuItem>
            <MenuItem icon={<ServerSVG />}>My Servers <NavLink to={"/servers"}/></MenuItem>
            <MenuItem icon={<AnalyticsIcon />}><NavLink to={"/analytics"}>Analytics</NavLink></MenuItem>
            <MenuItem icon={<SettingsIcon />}>Settings <NavLink to={"/settings"}/></MenuItem>
            <MenuItem icon={<PlaylistsIcon />}>Playlists <NavLink to="/playlists"/></MenuItem>
            <MenuItem icon={<GiftIcon />}>Language & Region <NavLink to="/language"/></MenuItem>
            <MenuItem icon={<PlaylistsIcon />}>Rank Card <NavLink to="/rankcard"/></MenuItem>
            <MenuItem icon={<DashboardIcon />}>Profile <NavLink to="/profile"/></MenuItem>
            <MenuItem icon={<DashboardIcon />}>Assets <NavLink to={"/assets"}/></MenuItem>
            <MenuItem icon={<NotificationsIcon />}>Notifications <NavLink to={"/notifications"}/></MenuItem>
            <MenuItem icon={<NotificationsIcon />}>Notification Settings <NavLink to={"/settings/notifications"}/></MenuItem>
            <MenuItem icon={<DashboardIcon />}>Placeholder</MenuItem>

            <MenuItem icon={<PaymentIcon />}>Terms of Use <NavLink to="/tos"/></MenuItem>
            <MenuItem icon={<PlaylistAltIcon />}>Privacy Policy <NavLink to="/privacy"/></MenuItem>
          </Menu>
        </SidebarContent>
        <SidebarFooter>
          <Menu className={`collapseSidebarWrapper ${collapsed ? "collapsed" : "expanded"}`} onClick={handleCollapsedChange}>
            <MenuItem icon={<CollapseIcon/>}>
              <span>Collapse Sidebar</span>
            </MenuItem>
          </Menu>
        </SidebarFooter>
      </ProSidebar>
      // <div>
      //
      //
      //
      //   {/*/!*<div style={{width: props.width}}>*!/*/}
      //   {/*/!*  <button onClick={props.openNav}>Open</button>*!/*/}
      //   {/*/!*  <button onClick={props.closeNav}>Close</button>*!/*/}
      //
      //   {/*<div className={"guildInfo"}>*/}
      //   {/*  <img src={guildLogo} alt={"Guild"}/>*/}
      //   {/*  <div className={"guildInfoText"}>*/}
      //   {/*    <h3 className={"guildName"}>General Testing Server</h3>*/}
      //   {/*    <h5 className={"memberCount"}>1,000,000 Members</h5>*/}
      //   {/*  </div>*/}
      //   {/*</div>*/}
      // </div>
  )
}

export default Sidebar