import React from 'react';
import {Route, Switch} from 'react-router-dom'
import PlaylistPage from "../../pages/playlists/PlaylistPage";
import QueuePage from "../../pages/QueuePage";
import CommandsPage from "../../pages/CommandsPage";
import NotFound from "../../pages/NotFound";
import MyServersPage from "../../pages/MyServersPage";
import PremiumPage from "../../pages/PremiumPage";
import SearchPage from "../../pages/SearchPage";
import HomePage from "../../pages/HomePage";
import BillingPage from "../../pages/BillingPage";
import AnalyticsPage from "../../pages/AnalyticsPage";
import TermsOfServicePage from "../../pages/legal/TermsOfServicePage";
import PrivacyPolicyPage from "../../pages/legal/PrivacyPolicyPage";
import RankCardPage from "../../pages/RankCardPage";
import LanguageRegionPage from "../../pages/Language&RegionPage";
import AssetsPage from "../../pages/AssetsPage";
import NotificationsPage from "../../pages/NotificationsPage";
import NotificationSettingsPage from "../../pages/NotificationSettingsPage";
import ProfilePage from "../../pages/ProfilePage";
import SettingsPage from "../../pages/SettingsPage";
import PlaylistOverviewPage from "../../pages/playlists/PlaylistOverviewPage";
import DashboardPage from "../../pages/DashboardPage";

const Content = () => {
  return (
      <section className="main">
        <div className="mainContent">
          <Switch>
            <Route path={"/"} exact component={HomePage}/>
            <Route path={"/home"} exact component={HomePage}/>
            <Route path={"/dashboard"} exact component={DashboardPage}/>
            <Route path={"/search"} exact component={SearchPage}/>
            <Route path={"/playlists"} exact component={PlaylistOverviewPage}/>
            <Route path={"/playlist/:id"} exact component={PlaylistPage}/>
            <Route path={"/queue"} exact component={QueuePage}/>
            <Route path={"/commands"} exact component={CommandsPage}/>
            <Route path={"/servers"} exact component={MyServersPage}/>
            <Route path={"/premium"} exact component={PremiumPage}/>
            <Route path={"/billing"} exact component={BillingPage}/>
            <Route path={"/analytics"} exact component={AnalyticsPage}/>
            <Route path={"/tos"} exact component={TermsOfServicePage}/>
            <Route path={"/privacy"} exact component={PrivacyPolicyPage}/>
            <Route path={"/rankcard"} exact component={RankCardPage}/>
            <Route path={"/language"} exact component={LanguageRegionPage}/>
            <Route path={"/assets"} exact component={AssetsPage}/>
            <Route path={"/profile"} exact component={ProfilePage}/>
            <Route path={"/settings"} exact component={SettingsPage}/>
            <Route path={"/notifications"} exact component={NotificationsPage}/>
            <Route path={"/settings/notifications"} exact component={NotificationSettingsPage}/>
            <Route component={NotFound}/>
          </Switch>
        </div>
      </section>
  )
}

export default Content