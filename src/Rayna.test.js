import { render, screen } from '@testing-library/react';
import Rayna from './Rayna';

test('renders learn react link', () => {
  render(<Rayna />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
